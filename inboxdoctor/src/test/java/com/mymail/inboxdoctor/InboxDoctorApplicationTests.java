package com.mymail.inboxdoctor;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class InboxDoctorApplicationTests {

	@Test
	public void contextLoads() {
	}

}
