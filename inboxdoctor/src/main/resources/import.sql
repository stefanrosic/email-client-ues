INSERT INTO rules (rule_id, _condition, _operation) VALUES (1, 0, 0);
INSERT INTO rules (rule_id, _condition, _operation) VALUES (2, 0, 1);
INSERT INTO rules (rule_id, _condition, _operation) VALUES (3, 0, 2);
INSERT INTO rules (rule_id, _condition, _operation) VALUES (4, 1, 0);
INSERT INTO rules (rule_id, _condition, _operation) VALUES (5, 1, 1);
INSERT INTO rules (rule_id, _condition, _operation) VALUES (6, 1, 2);
INSERT INTO rules (rule_id, _condition, _operation) VALUES (7, 2, 0);
INSERT INTO rules (rule_id, _condition, _operation) VALUES (8, 2, 1);
INSERT INTO rules (rule_id, _condition, _operation) VALUES (9, 2, 2);
INSERT INTO rules (rule_id, _condition, _operation) VALUES (10, 3, 0);
INSERT INTO rules (rule_id, _condition, _operation) VALUES (11, 3, 1);
INSERT INTO rules (rule_id, _condition, _operation) VALUES (12, 3, 3);

INSERT INTO photos (photo_id, path) VALUES (2, 'rosic.jpg');

INSERT INTO users (user_id, username, password, first_name, last_name) VALUES (1, 'inbox', '$2a$10$iFxXEF4uAzNPOql1aCMyr.fY5IpmuLfrzg3R5r9XR8OuMLIxyu0/S', 'Доктори', 'Инбокса');
INSERT INTO users (user_id, username, password, first_name, last_name) VALUES (2, 'photo_loader', '$2a$10$27MzCf6uOXaWCHpeHDQJx.4IrbYbMjQ5ALn8KDOOuATrEgqghwFY.', 'Only', 'Photo');

INSERT INTO authority VALUES (1, 'USER');

INSERT INTO user_authority (user_id, authority_id) VALUES (1, 1);

INSERT INTO contacts (contact_id, display_name, email, first_name, last_name, note, format, photo_id, _user) VALUES (1, 'Stefan', 'stefanrosic2@gmail.com', 'Stefan', 'Rosic', 'Ovo je moj prvi kreirani kontakt', 0, 2, 1);

INSERT INTO accounts (account_id, password, pop3imap, smtp, username, _user) VALUES (1, 'inboxdoctor1389', 'imap.gmail.com:993', 'smtp.gmail.com:465', 'inboxdoctorsrb@gmail.com', 1);
INSERT INTO accounts (account_id, password, pop3imap, smtp, username, _user) VALUES (2, 'projekat2019', 'imap.gmail.com:993', 'smtp.gmail.com:465', 'inbox.doctor.milan@gmail.com', 1);

INSERT INTO folders (folder_id, name, destination_id, parent_folder, account_id) VALUES (1, 'Пријемно сандуче', 1, NULL, 1);
INSERT INTO folders (folder_id, name, destination_id, parent_folder, account_id) VALUES (2, 'Послане', 1, NULL, 1);
INSERT INTO folders (folder_id, name, destination_id, parent_folder, account_id) VALUES (3, 'Нацрти', 1, NULL, 1);

INSERT INTO folders (folder_id, name, destination_id, parent_folder, account_id) VALUES (4, 'Пријемно сандуче', 1, NULL, 2);
INSERT INTO folders (folder_id, name, destination_id, parent_folder, account_id) VALUES (5, 'Послане', 1, NULL, 2);
INSERT INTO folders (folder_id, name, destination_id, parent_folder, account_id) VALUES (6, 'Нацрти', 1, NULL, 2);