package com.mymail.inboxdoctor.repository;

import com.mymail.inboxdoctor.entity.Message;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

@Repository
public interface MessageRepository extends JpaRepository<Message, Integer> {

    @Query(value = "SELECT MAX(date_time) FROM messages m WHERE folder = ?1", nativeQuery = true)
    Date findLastDate(int folderId);

    @Query(value = "SELECT * FROM messages m WHERE m._read = false AND m.account = ?1", nativeQuery = true)
    List<Message> findUnreadMessages(int accountId);

    @Query(value = "SELECT * FROM messages m WHERE m.account = ?1 AND m.folder = ?2", nativeQuery = true)
    List<Message> findAllByAccountIdAndFolderId(int accountId, int folderId);

    @Query(value = "DELETE FROM messages m WHERE m.account = ?1", nativeQuery = true)
    void removeMessagesByAccount(int accountId);

    @Transactional
    @Modifying(clearAutomatically = true)
    @Query(value = "UPDATE messages SET _read = true WHERE messages.message_id = ?1", nativeQuery = true)
    void setRead(int messageID);

    @Query(value = "SELECT * FROM messages WHERE content LIKE %?1% AND account = ?2", nativeQuery = true)
    List<Message> findMessageForSearch(@Param("search_value") String search_value, @Param("search_value") int acc);
    
    @Query(value = "SELECT * FROM messages WHERE subject LIKE %?1% AND account = ?2", nativeQuery = true)
    List<Message> findMessageForSearchSubject(@Param("search_value") String search_value, @Param("search_value") int acc);

    @Query(value = "SELECT * FROM messages m JOIN messages_bcc mbcc ON mbcc.message_message_id = m.message_id JOIN contacts c ON c.contact_id = mbcc.bcc_contact_id WHERE c.email LIKE %?1% AND account = ?2", nativeQuery = true)
    List<Message> findMessageWithEmailInBCC(@Param("search_value") String search_value, @Param("search_value") int acc);

    @Query(value = "SELECT * FROM messages m JOIN messages_cc mcc ON mcc.message_message_id = m.message_id JOIN contacts c ON c.contact_id = mcc.cc_contact_id WHERE c.email LIKE %?1% AND account = ?2", nativeQuery = true)
    List<Message> findMessageWithEmailInCC(@Param("search_value") String search_value, @Param("search_value") int acc);

    @Query(value = "SELECT * FROM messages m JOIN messages_to mto ON mto.message_message_id = m.message_id JOIN contacts c ON c.contact_id = mto.to_contact_id WHERE c.email LIKE %?1% AND account = ?2", nativeQuery = true)
    List<Message> findMessageWithEmailInTo(@Param("search_value") String search_value, @Param("search_value") int acc);

    @Query(value = "SELECT * FROM messages m JOIN messages_tags mtags ON mtags.message_message_id = m.message_id JOIN tags t ON mtags.tags_tag_id = t.tag_id WHERE t.name LIKE %?1% AND account = ?2", nativeQuery = true)
    List<Message> findMessageWithTag(@Param("search_value") String search_value, @Param("search_value") int acc);

    @Query(value = "SELECT * FROM messages m JOIN contacts c WHERE m._from = c.contact_id AND c.email LIKE %?1% AND account = ?2", nativeQuery = true)
    List<Message> findMessageFrom(@Param("search_value") String search_value, @Param("search_value") int acc);


}
