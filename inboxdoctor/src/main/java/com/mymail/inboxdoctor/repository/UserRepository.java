package com.mymail.inboxdoctor.repository;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.mymail.inboxdoctor.entity.User;

@Repository
public interface UserRepository extends JpaRepository<User, Integer>{

	User findByUsername(String username);

	@Transactional
	@Modifying
	@Query(value = "INSERT INTO user_authority (user_id, authority_id) values (:user_id, :auth_id)", nativeQuery = true)
	void insertAuthority(@Param("user_id") int user_id, @Param("auth_id") int auth_id);

}
