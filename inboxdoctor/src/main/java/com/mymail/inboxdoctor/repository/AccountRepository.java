package com.mymail.inboxdoctor.repository;

import com.mymail.inboxdoctor.entity.Account;
import com.mymail.inboxdoctor.entity.User;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AccountRepository extends JpaRepository<Account, Integer> {
	
    List<Account> findAllByUser(User user);

}
