package com.mymail.inboxdoctor.repository;

import com.mymail.inboxdoctor.entity.Contact;
import com.mymail.inboxdoctor.entity.User;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface ContactRepository extends JpaRepository<Contact, Integer> {
	
	List<Contact> findAllByUser(User user);

	@Query(value = "SELECT * FROM contacts c WHERE c.`_user` = ?1", nativeQuery = true)
	List<Contact> findAllByUserId(int userId);

}
