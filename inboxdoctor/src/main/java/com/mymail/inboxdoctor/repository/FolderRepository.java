package com.mymail.inboxdoctor.repository;

import com.mymail.inboxdoctor.entity.Folder;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface FolderRepository extends JpaRepository<Folder, Integer> {

    List<Folder> findFoldersByAccount_Id(int accountId);

    @Query(value = "SELECT * FROM folders f WHERE f.account_id = ?1 AND f.name = 'Пријемно сандуче'", nativeQuery = true)
    Folder findInboxFolderByAccount(int accountId);

    @Query(value = "SELECT * FROM folders f WHERE f.account_id = ?1 AND f.name = 'Послане'", nativeQuery = true)
    Folder findSentFolderByAccount(int accountId);

    @Query(value = "SELECT * FROM folders f WHERE f.account_id = ?1 AND f.name = 'Нацрти'", nativeQuery = true)
    Folder findDraftFolderByAccount(int accountId);
}
