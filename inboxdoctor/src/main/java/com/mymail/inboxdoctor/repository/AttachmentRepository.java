package com.mymail.inboxdoctor.repository;

import com.mymail.inboxdoctor.entity.Attachment;
import org.springframework.data.jpa.repository.JpaRepository;

import org.springframework.stereotype.Repository;

@Repository
public interface AttachmentRepository extends JpaRepository<Attachment, Integer> {
}
