package com.mymail.inboxdoctor.repository;

import com.mymail.inboxdoctor.entity.Tag;
import org.springframework.stereotype.Repository;
import org.springframework.data.jpa.repository.JpaRepository;

@Repository
public interface TagRepository extends JpaRepository<Tag, Integer> {
}
