package com.mymail.inboxdoctor.controller;

import com.mymail.inboxdoctor.entity.Photo;
import com.mymail.inboxdoctor.service.PhotoServiceInterface;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Paths;
import java.security.Principal;
import java.util.List;
import java.util.Objects;

@RestController
@RequestMapping("/api")
public class PhotoController {

    private final String PHOTOS_FOLDER_PATH = Objects.requireNonNull(getClass()
            .getClassLoader()
            .getResource(""))
            .getPath().substring(1)
            + ".." + File.separator
            + ".." + File.separator
            + "src" + File.separator
            + "main" + File.separator
            + "photos" + File.separator;

    private final PhotoServiceInterface photoService;

    public PhotoController(PhotoServiceInterface photoService) {
        this.photoService = photoService;
    }

    @RequestMapping(value = "/photos", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Photo>> getPhotos() {
        List<Photo> photos = photoService.findAll();
        return new ResponseEntity<>(photos, HttpStatus.OK);
    }

    @RequestMapping(value = "/photos/upload", method = RequestMethod.POST)
    public ResponseEntity<Object> upload(@RequestParam MultipartFile file, Principal principal) throws IOException {
    	System.out.println("WWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWW");
        File convertFile = new File(PHOTOS_FOLDER_PATH + file.getOriginalFilename());

        FileOutputStream fileOutputStream = new FileOutputStream(convertFile);
        fileOutputStream.write(file.getBytes());
        fileOutputStream.close();

        file.transferTo(Paths.get(PHOTOS_FOLDER_PATH + file.getOriginalFilename()));

        Photo temp = photoService.save(new Photo(file.getOriginalFilename()));

        return new ResponseEntity<>(temp, HttpStatus.OK);

    }
}
