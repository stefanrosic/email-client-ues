package com.mymail.inboxdoctor.controller;

import com.mymail.inboxdoctor.dto.ContactDTO;
import com.mymail.inboxdoctor.dto.ContactWListsDTO;
import com.mymail.inboxdoctor.entity.Contact;
import com.mymail.inboxdoctor.entity.User;
import com.mymail.inboxdoctor.lucene.indexer.Indexer;
import com.mymail.inboxdoctor.service.ContactServiceInterface;

import com.mymail.inboxdoctor.service.UserServiceInterface;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.security.Principal;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

@RestController
@RequestMapping(value = "/api")
public class ContactController {

    private final ContactServiceInterface contactService;
	private final UserServiceInterface userService;

    public ContactController(ContactServiceInterface contactService,
                             UserServiceInterface userService) {
        this.contactService = contactService;
        this.userService = userService;
    }

    @RequestMapping(value = "/contacts", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<ContactWListsDTO>> getContacts(Principal principal) throws IOException {
		User user = userService.findByUsername(principal.getName());    	
		
        List<Contact> contacts = contactService.findAllByUserId(user.getId());
        List<ContactWListsDTO> contactDTOS = new ArrayList<>();

        for (Contact contact : contacts) {
            contactDTOS.add(new ContactWListsDTO(contact));
            if(!Indexer.existContact(contact.getId())){
                Indexer.addContact(new ContactDTO(contact));
            }
        }

        return new ResponseEntity<>(contactDTOS, HttpStatus.OK);
    }

    @RequestMapping(value = "/contacts/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ContactDTO> getContact(@PathVariable("id") int id, Principal principal) {
        Contact temp = contactService.findOne(id);

        if (temp == null)
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        ContactDTO tempDTO = new ContactDTO(temp);
        return new ResponseEntity<>(tempDTO, HttpStatus.OK);
    }

    @RequestMapping(value = "/contacts/add", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ContactDTO> addContact(@RequestBody Contact contact, Principal principal) throws ExecutionException, InterruptedException {
		User user = userService.findByUsername(principal.getName());    	
		contact.setUser(user);
		
        contact = contactService.save(contact);
        if (contact == null)
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        ContactDTO contactDTO = new ContactDTO(contact);
        Indexer.addContact(contactDTO);
        return new ResponseEntity<>(contactDTO, HttpStatus.OK);
    }

    @RequestMapping(value = "/contacts/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Contact> deleteContact(@PathVariable("id") int id, Principal principal) {
        contactService.remove(id);
        Indexer.deleteContact(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @RequestMapping(value = "/contacts", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ContactDTO> updateContact(@RequestBody Contact contact, Principal principal) {
		User user = userService.findByUsername(principal.getName());
		contact.setUser(user);

        contactService.save(contact);
        ContactDTO contactDTO = new ContactDTO(contact);
        Indexer.deleteContact(contactDTO.getId());
        Indexer.addContact(contactDTO);
        return new ResponseEntity<>(contactDTO, HttpStatus.OK);
    }
}
