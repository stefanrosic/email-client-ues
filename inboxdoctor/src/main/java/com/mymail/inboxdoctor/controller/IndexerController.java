package com.mymail.inboxdoctor.controller;

import com.mymail.inboxdoctor.lucene.indexer.Indexer;
import com.mymail.inboxdoctor.service.AttachmentService;
import com.mymail.inboxdoctor.service.ContactService;
import com.mymail.inboxdoctor.service.MessageService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.*;

@RestController
@RequestMapping("/api")
public class IndexerController {
	
	@Autowired
	ContactService contactService;

    @Autowired
    MessageService messageService;

    @Autowired
    AttachmentService attachmentService;

    @GetMapping("/reindex-messages")
    public ResponseEntity<String> indexMessages() throws IOException {
        long start = new Date().getTime();
        int numIndexed = Indexer.getInstance().indexMessagess(messageService.findAll());
        long end = new Date().getTime();
        String text = "Indexing " + numIndexed + " object took "
                + (end - start) + " milliseconds";
        return new ResponseEntity<String>(text, HttpStatus.OK);
    }
    
    @GetMapping("/reindex-contact")
    public ResponseEntity<String> indexContacts() throws IOException {
        long start = new Date().getTime();
        int numIndexed = Indexer.getInstance().indexContacts(contactService.findAll());
        long end = new Date().getTime();
        String text = "Indexing " + numIndexed + " object took "
                + (end - start) + " milliseconds";
        return new ResponseEntity<String>(text, HttpStatus.OK);
    }

    @GetMapping("/reindex-attachment")
    public ResponseEntity<String> indexAttachment() throws IOException {
        long start = new Date().getTime();
        int numIndexed = Indexer.getInstance().indexAttachments(attachmentService.findAll());
        long end = new Date().getTime();
        String text = "Indexing " + numIndexed + " object took "
                + (end - start) + " milliseconds";
        return new ResponseEntity<String>(text, HttpStatus.OK);
    }
}
