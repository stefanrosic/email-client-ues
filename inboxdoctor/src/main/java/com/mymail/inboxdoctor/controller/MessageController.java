package com.mymail.inboxdoctor.controller;

import java.io.*;
import java.net.MalformedURLException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.Principal;
import java.util.*;

import com.mymail.inboxdoctor.dto.*;
import com.mymail.inboxdoctor.lucene.indexer.Indexer;
import com.mymail.inboxdoctor.mailApi.MailSender;
import com.mymail.inboxdoctor.entity.*;
import com.mymail.inboxdoctor.service.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@RestController
@RequestMapping(value = "/api")
public class MessageController {

    private final UserServiceInterface userService;
    private final MessageServiceInterface messageService;
    private final ContactServiceInterface contactService;
    private final TagServiceInterface tagService;
    private final AttachmentServiceInterface attachmentService;
    private final FolderServiceInterface folderService;
    private static final Logger log = LoggerFactory.getLogger(MessageController.class);

    public MessageController(MessageServiceInterface messageService,
                             ContactServiceInterface contactService,
                             TagServiceInterface tagService,
                             AttachmentServiceInterface attachmentService,
                             UserServiceInterface userService,
                             FolderServiceInterface folderService) {
        this.messageService = messageService;
        this.contactService = contactService;
        this.tagService = tagService;
        this.attachmentService = attachmentService;
        this.userService = userService;
        this.folderService = folderService;
    }

    @RequestMapping(value = "/inbox/emails", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<MessageListDTO>> getInboxEmails(Principal principal) throws IOException {
        try {
            User user = User.getLoggedUser(principal, userService);
            Account account = user.getUserCurrentAccount();

            log.info("Current account: " + account.getUsername());

            Folder inbox = folderService.findInboxFolderByAccount(account.getId());

            List<Message> emails = messageService.findAllByAccountIdAndFolderId(account.getId(), inbox.getId());
            List<MessageListDTO> emailDTOS = new ArrayList<>();

            for (Message email : emails) {
                emailDTOS.add(new MessageListDTO(email));
                if(!Indexer.existMessage(email.getId())){
                    Indexer.addMessage(new MessageDTO(email));
                }
                if(email.getAttachments().size() != 0) {
                    Attachment attachment = email.getAttachments().iterator().next();
                    if (!Indexer.existAttachment(attachment.getId())){
                        Indexer.addAttachment(new AttachmentDTO(attachment));
                    }
                }
            }

            return new ResponseEntity<>(emailDTOS, HttpStatus.OK);
        } catch (Exception e) {
        	e.printStackTrace();
            log.error("Trying unauthorized access");
        }

        return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
    }

    @RequestMapping(value = "/emails/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<MessageWListsDTO> getEmail(@PathVariable("id") int id, Principal principal) {
        Message temp = messageService.findOne(id);
        if (temp == null)
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        MessageWListsDTO tempDTO = new MessageWListsDTO(temp);
        return new ResponseEntity<>(tempDTO, HttpStatus.OK);
    }

    @RequestMapping(value = "/emails/add", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<MessageDTO> addEmail(@RequestBody Message message, Principal principal) {
        try {
            User user = User.getLoggedUser(principal, userService);
            Account account = user.getUserCurrentAccount();

            message.setAccount(account);

            Folder sent = folderService.findSentFolderByAccount(account.getId());

            message.setFolder(sent);
            message.setDateTime(new Date());
            message.setRead(true);

            MailSender mailSender = new MailSender(messageService, contactService, tagService, attachmentService, folderService);
            mailSender.sendEmail(message, user);

            return new ResponseEntity<>(new MessageDTO(message), HttpStatus.OK);
        } catch (Exception e) {
            log.error("Trying unauthorized access");
        }
        return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
    }

    @RequestMapping(value = "/emails/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Message> deleteMessage(@PathVariable("id") int id, Principal principal) {
        messageService.remove(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @RequestMapping(value = "/emails/unreadMessages", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<MessageDTO>> getUnreadMessages(Principal principal) {
        try {
            User user = User.getLoggedUser(principal, userService);
            Account account = user.getUserCurrentAccount();
            List<Message> unreadEmails = messageService.findUnreadMessages(account.getId());
            List<MessageDTO> emailDTOS = new ArrayList<>();
            for (Message email : unreadEmails) {
                emailDTOS.add(new MessageDTO(email));
            }
            return new ResponseEntity<>(emailDTOS, HttpStatus.OK);
        } catch (Exception e) {
            log.error("Trying unauthorized access");
        }
        return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
    }

    @RequestMapping(value = "/emails/setRead/{id}", method = RequestMethod.PUT)
    public ResponseEntity<MessageDTO> setReadMessage(@PathVariable("id") int id, Principal principal) {
        messageService.setRead(id);
        MessageDTO messageDTO = new MessageDTO(messageService.findOne(id));
        return new ResponseEntity<>(messageDTO, HttpStatus.OK);
    }

    @RequestMapping(value = "/search/{search_value}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<MessageDTO>> searchMessages(@PathVariable("search_value") String search_value, Principal principal) {
        User user = User.getLoggedUser(principal, userService);
        Account account = user.getUserCurrentAccount();
        
        List<Message> filtered = new ArrayList<>();

        List<Message> filteredMessages = messageService.findMessagesByPassedValue(search_value, account.getId());
        List<Message> filteredMessagesBCC = messageService.findMessageWithEmailInBCC(search_value, account.getId());
        List<Message> filteredMessagesSubject = messageService.findMessagesByPassedValueSubject(search_value, account.getId());
        List<Message> filteredMessagesCC = messageService.findMessageWithEmailInCC(search_value, account.getId());
        List<Message> filteredMessagesTo = messageService.findMessageWithEmailInTo(search_value, account.getId());
        List<Message> filteredMessagesTags = messageService.findMessageWithTag(search_value, account.getId());
        List<Message> filteredMessagesFrom = messageService.findMessageFrom(search_value, account.getId());

        filtered.addAll(filteredMessages);
        filtered.addAll(filteredMessagesSubject);
        filtered.addAll(filteredMessagesBCC);
        filtered.addAll(filteredMessagesCC);
        filtered.addAll(filteredMessagesTo);
        filtered.addAll(filteredMessagesTags);
        filtered.addAll(filteredMessagesFrom);
        
        List<MessageDTO> filterMessagesDTO = new ArrayList<>();
        for (Message email : filtered) {
            if (filterMessagesDTO.stream().filter(m -> m.getId() == email.getId()).findFirst().orElse(null) == null)
                filterMessagesDTO.add(new MessageDTO(email));
        }

        return new ResponseEntity<>(filterMessagesDTO, HttpStatus.OK);
    }

    @GetMapping("/message/{fileName}")
    public ResponseEntity<Resource> getFile(@PathVariable String fileName, HttpServletRequest request, HttpServletResponse response, Principal principal) throws Exception {
        Resource file = getFileAsResource(fileName);
        HttpHeaders headers = prepareHeaderForFileReturn(fileName, request, response);
        return new ResponseEntity<Resource>(file, headers, HttpStatus.OK);
    }

    private HttpHeaders prepareHeaderForFileReturn(String fileName, HttpServletRequest request,
                                                   HttpServletResponse response) {
        HttpHeaders headers = new HttpHeaders();
        headers.set(HttpHeaders.CONTENT_TYPE, getContentTypeForAttachment(fileName));
        response.setHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");
        return headers;
    }

    public Resource getFileAsResource(String filename) throws FileNotFoundException {
        String filePath = "src/main/resources/files" +  "/" + filename;
        Resource file = loadAsResource(filePath);
        return file;
    }

    private Resource loadAsResource(String filename) throws FileNotFoundException {
        try {
            Path file = Paths.get(filename);
            org.springframework.core.io.Resource resource = new UrlResource(file.toUri());
            if (resource.exists() || resource.isReadable()) {
                return resource;
            } else {
                log.error("Could not read file: " + filename);
                throw new FileNotFoundException();
            }
        } catch (MalformedURLException e) {
            log.error("Could not read file: " + filename, e);
            throw new FileNotFoundException();
        }
    }

    public String getContentTypeForAttachment(String fileName) {
        String fileExtension = com.google.common.io.Files.getFileExtension(fileName);
        if (fileExtension.equals("pdf")) return "application/pdf";
        else if (fileExtension.equals("doc")) return "application/msword";
        else if (fileExtension.equals("jpeg")) return "image/jpeg";
        return "application/pdf";
    }

    @RequestMapping(value = "/emails/get-attachment/{id}", method = RequestMethod.GET)
    public ResponseEntity<AttachmentsDTO> getAttachment(@PathVariable("id") int id, Principal principal) {
        Message m = messageService.findOne(id);
        AttachmentsDTO attachmentsDTO = null;
        try {
            Attachment attachment = attachmentService.findOne(m.getAttachments().iterator().next().getId());
            attachmentsDTO = new AttachmentsDTO(attachment);
        }catch(Exception ex){
            log.error("Selected message has not attachment!");
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(attachmentsDTO, HttpStatus.OK);
    }

}
