package com.mymail.inboxdoctor.controller;

import com.mymail.inboxdoctor.dto.FolderDTO;
import com.mymail.inboxdoctor.dto.FolderWListsDTO;
import com.mymail.inboxdoctor.dto.MessageDTO;
import com.mymail.inboxdoctor.entity.*;
import com.mymail.inboxdoctor.service.ContactServiceInterface;
import com.mymail.inboxdoctor.service.FolderServiceInterface;
import com.mymail.inboxdoctor.service.MessageServiceInterface;
import com.mymail.inboxdoctor.service.UserServiceInterface;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

@RestController
@RequestMapping(value = "/api")
public class FolderController {

    private final FolderServiceInterface folderService;
    private final MessageServiceInterface messageService;
    private final ContactServiceInterface contactService;
    private final UserServiceInterface userService;
    private static final Logger log = LoggerFactory.getLogger(AccountController.class);

    public FolderController(FolderServiceInterface folderService,
                            MessageServiceInterface messageService,
                            ContactServiceInterface contactService,
                            UserServiceInterface userService) {
        this.folderService = folderService;
        this.messageService = messageService;
        this.contactService = contactService;
        this.userService = userService;
    }

    @RequestMapping(value = "/folders", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<FolderWListsDTO>> getFolders(Principal principal) {
        try {
            User user = userService.findByUsername(principal.getName());
            Account account = user.getUserCurrentAccount();
            List<Folder> folders = folderService.findFoldersByAccount_Id(account.getId());
            List<FolderWListsDTO> folderDTOS = new ArrayList<>();
            folders.forEach(f -> folderDTOS.add(new FolderWListsDTO(f)));
            return new ResponseEntity<>(folderDTOS, HttpStatus.OK);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }

    @RequestMapping(value = "/folders/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<FolderDTO> getFolder(@PathVariable("id") int id) {
        Folder temp = folderService.findOne(id);
        if (temp == null)
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        FolderDTO tempDTO = new FolderDTO(temp);
        return new ResponseEntity<>(tempDTO, HttpStatus.OK);
    }

    @RequestMapping(value = "/folders/add/{text}", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<FolderDTO> addFolder(@RequestBody Folder folder, Principal principal, @PathVariable("text") String text) {

        User user = User.getLoggedUser(principal, userService);
        Account account = user.getUserCurrentAccount();
        folder.setId(folderService.findAll().size() + 1);
        folder.setAccount(account);
        folder.setMessages(new HashSet<Message>());
        folder = folderService.save(folder);
        System.out.println(folder.toString());
        if (folder == null)
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        ECondition condition = folder.getDestination().getCondition();
        EOperation operation = folder.getDestination().getOperation();
        System.out.println(folder.getDestination().toString());
        if (condition.equals(ECondition.TO) && operation.equals(EOperation.MOVE)) {
            toMove(folder, text, userService.findByUsername(principal.getName()).getId());
        }
        if (condition.equals(ECondition.FROM) && operation.equals(EOperation.MOVE)) {
            fromMove(folder, text, userService.findByUsername(principal.getName()).getId());
        }
        if (condition.equals(ECondition.SUBJECT) && operation.equals(EOperation.MOVE)) {
            subjectMove(folder, text, userService.findByUsername(principal.getName()).getId());
        }
        if (condition.equals(ECondition.SUBJECT) && operation.equals(EOperation.COPY)) {
            subjectCopy(folder, text, userService.findByUsername(principal.getName()).getId());
        }
        if (condition.equals(ECondition.FROM) && operation.equals(EOperation.COPY)) {
            FromCopy(folder, text, userService.findByUsername(principal.getName()).getId());
        }
        if (condition.equals(ECondition.TO) && operation.equals(EOperation.COPY)) {
            ToCopy(folder, text, userService.findByUsername(principal.getName()).getId());
        }
        if (condition.equals(ECondition.CC) && operation.equals(EOperation.MOVE)) {
            ccMove(folder, text, userService.findByUsername(principal.getName()).getId());
        }
        if (condition.equals(ECondition.CC) && operation.equals(EOperation.COPY)) {
            ccCopy(folder, text, userService.findByUsername(principal.getName()).getId());
        }

        FolderDTO tempDTO = new FolderDTO(folder);
        return new ResponseEntity<>(tempDTO, HttpStatus.OK);
    }


    @RequestMapping(value = "/folder/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Folder> deleteFolder(@PathVariable("id") int id, Principal principal) {
        folderService.remove(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @RequestMapping(value = "/folders/{id}", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<FolderDTO> updateFolder(@PathVariable("id") int id, @RequestBody Folder folder, Principal principal) {
        try {
            User user = User.getLoggedUser(principal, userService);
            Account account = user.getUserCurrentAccount();
            folder.setId(id);
            folder.setAccount(account);
            folderService.save(folder);
            if (id != folder.getId())
                return new ResponseEntity<>(HttpStatus.BAD_REQUEST);

            FolderDTO tempDTO = new FolderDTO(folder);
            return new ResponseEntity<>(tempDTO, HttpStatus.OK);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }

    @RequestMapping(value = "/folders/draft/addMessage", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Object>> addDraft(@RequestBody Message message, Principal principal) {
        try {
            User user = User.getLoggedUser(principal, userService);
            Account account = user.getUserCurrentAccount();
            Folder draft = folderService.findDraftFolderByAccount(account.getId());
            message.setFolder(draft);
            contactService.save(message.getFrom());
            messageService.save(message);
            FolderDTO draftDTO = new FolderDTO(draft);
            MessageDTO messageDTO = new MessageDTO(message);
            List<Object> retVal = new ArrayList<>();
            retVal.add(messageDTO);
            retVal.add(draftDTO);
            return new ResponseEntity<>(retVal, HttpStatus.OK);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }

    public void toMove(Folder folder, String text, int id) {
        for (Contact con : contactService.findAll()) {
            System.out.println("ConSvi" + con);
            if (con.getEmail().equals(text)) {
                System.out.println("ConProsli" + con);
                for (Message me : messageService.findAll()) {
                    for (Contact msgCon : me.getTo()) {
                        if (msgCon.getEmail().equals(con.getEmail()) && me.getAccount().getId() == id) {
                            me.setFolder(folder);
                            messageService.save(me);
                        }
                    }
                }
            }
        }
    }

    public void ccMove(Folder folder, String text, int id) {
        for (Contact con : contactService.findAll()) {
            System.out.println("ConSVIcc" + con);
            if (con.getEmail().equals(text)) {
                System.out.println("ConProsliCC" + con);
                for (Message me : messageService.findAll()) {
                    for (Contact msgCon : me.getCc()) {
                        if (msgCon.getEmail().equals(text) && me.getAccount().getId() == id) {
                            me.setFolder(folder);
                            messageService.save(me);
                        }
                    }
                }
            }
        }
    }

    public void fromMove(Folder folder, String text, int id) {
        for (Message m : messageService.findAll()) {
            System.out.println("msgBasse " + m.getFrom().getEmail());
            System.out.println("msgFol " + text);
            if (m.getFrom().getEmail().equals(text) && m.getAccount().getId() == id) {
                m.setFolder(folder);
                messageService.save(m);
            }
        }

    }

    public void subjectMove(Folder folder, String text, int id) {
        for (Message m : messageService.findAll()) {
            System.out.println("msgBasse " + m.getSubject());
            System.out.println("msgFol " + text);
            if (m.getSubject().equals(text) && m.getAccount().getId() == id) {
                m.setFolder(folder);
                messageService.save(m);

            }
        }
    }

    public void removeLast() {
        messageService.remove(messageService.findAll().size());
    }

    public void subjectCopy(Folder folder, String text, int id) {
        for (Message m : messageService.findAll()) {
            System.out.println("msgBasse " + m.getSubject());
            System.out.println("msgFol " + text);
            Message newMessage = new Message();
            if (m.getSubject().equals(text)) {
                newMessage = m;
                newMessage.setId(messageService.findAll().size() + 1);
                newMessage.setFolder(folder);
                messageService.save(newMessage);
            }
        }
    }

    public void FromCopy(Folder folder, String text, int id) {
        for (Message m : messageService.findAll()) {
            System.out.println("msgBasse " + m.getFrom().getEmail());
            System.out.println("msgFol " + text);
            Message newMessage = new Message();
            if (m.getFrom().getEmail().equals(text)) {
                newMessage = m;
                newMessage.setFolder(folder);
                newMessage.setId(messageService.findAll().size() + 1);
                messageService.save(newMessage);
            }
        }
    }

    public void ToCopy(Folder folder, String text, int id) {
        for (Contact con : contactService.findAll()) {
            System.out.println("ConSviToCopy" + con);
            if (con.getEmail().equals(text)) {
                System.out.println("ConProsliToCopy" + con);
                for (Message me : messageService.findAll()) {
                    for (Contact msgCon : me.getTo()) {
                        System.out.println("Sve poruke  " + me.stringMethod());
                        if (msgCon.getEmail().equals(text) && me.getAccount().getId() == id) {
                            System.out.println("ODGG poruke  " + me.stringMethod());
                            me.setFolder(folder);
                            messageService.save(me);
                        }
                    }
                }
            }
        }
    }

    public void ccCopy(Folder folder, String text, int id) {
        for (Contact con : contactService.findAll()) {
            System.out.println("ConSvicopyCC" + con);
            if (con.getEmail().equals(text)) {
                System.out.println("ConPROSLI" + con);
                for (Message me : messageService.findAll()) {
                    for (Contact msgCon : me.getCc()) {
                        if (msgCon.getEmail().equals(text) && me.getAccount().getId() == id) {
                            me.setFolder(folder);
                            messageService.save(me);
                        }
                    }
                }
            }
        }
    }
}
