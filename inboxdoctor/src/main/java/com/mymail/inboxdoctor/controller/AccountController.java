package com.mymail.inboxdoctor.controller;

import com.mymail.inboxdoctor.dto.AccountDTO;
import com.mymail.inboxdoctor.entity.Account;
import com.mymail.inboxdoctor.entity.Folder;
import com.mymail.inboxdoctor.entity.Rule;
import com.mymail.inboxdoctor.entity.User;
import com.mymail.inboxdoctor.service.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping(value = "/api")
public class AccountController {

    private final AccountServiceInterface accountService;
	private final UserServiceInterface userService;
	private final FolderServiceInterface folderService;
	private final RuleServiceInterface ruleService;
	private final MessageServiceInterface messageService;
    private static final Logger log = LoggerFactory.getLogger(AccountController.class);

    public AccountController(AccountServiceInterface accountService,
                             UserServiceInterface userService,
                             FolderServiceInterface folderService,
                             RuleServiceInterface ruleService,
                             MessageServiceInterface messageService) {
        this.accountService = accountService;
        this.userService = userService;
        this.folderService = folderService;
        this.ruleService = ruleService;
        this.messageService = messageService;
    }

    @RequestMapping(value = "/logout", method = RequestMethod.GET)
    public ResponseEntity doLogout() {
        return ResponseEntity.status(200).build();
    }

    @RequestMapping(value = "/accounts", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<AccountDTO>> getAccounts(Principal principal) {
        User user = User.getLoggedUser(principal, userService);
        List<Account> accounts = accountService.findAllByUser(user);
        List<AccountDTO> accountDTOS = new ArrayList<>();
        for (Account account : accounts
        ) {
            accountDTOS.add(new AccountDTO(account));
            log.info("Account: " + account.getUsername());
        }
        return new ResponseEntity<>(accountDTOS, HttpStatus.OK);
    }

    @RequestMapping(value = "/accounts/add", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<AccountDTO> addAccount(Principal principal, @RequestBody Account account) {
        try {
            User user = User.getLoggedUser(principal, userService);
            account.setUser(user);
            Rule rule = ruleService.findOne(1);
            log.info("RuleID: " + rule.getId());
            folderService.save(new Folder("Пријемно сандуче", rule, account));
            folderService.save(new Folder("Послане", rule, account));
            folderService.save(new Folder("Нацрти", rule, account));
            accountService.save(account);
            return new ResponseEntity<>(new AccountDTO(account), HttpStatus.OK);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }

    @RequestMapping(value = "/accounts/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Boolean> deleteAccount(@PathVariable("id") int id, Principal principal) {
        try {
            User user = User.getLoggedUser(principal, userService);
            user.setUserCurrentAccount(null);
            userService.save(user);
            accountService.remove(id);
            return new ResponseEntity<>(true, HttpStatus.OK);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return new ResponseEntity<>(false, HttpStatus.BAD_REQUEST);
    }

    @RequestMapping(value = "/accounts/set", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<AccountDTO> setCurrentAccount(@RequestBody Account acc, Principal principal) {
        try {
            User user = User.getLoggedUser(principal, userService);
            user.setUserCurrentAccount(accountService.findOne(acc.getId()));
            userService.save(user);            
            AccountDTO account = new AccountDTO(user.getUserCurrentAccount());
            return new ResponseEntity<>(account, HttpStatus.OK);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }
    
    @RequestMapping(value = "/account/update", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<AccountDTO> updateAccount(@RequestBody Account account, Principal principal){
    	User user = userService.findByUsername(principal.getName());
    	Account accountByID = accountService.findOne(account.getId());

    	if (account.getPassword() == null || account.getPassword().equals(""))
    	    account.setPassword(accountByID.getPassword());
    	
    	account.setUser(user);
    	accountService.save(account);
        AccountDTO AccountDTO = new AccountDTO(account);
        return new ResponseEntity<>(AccountDTO, HttpStatus.OK);
    }



}

