package com.mymail.inboxdoctor.controller;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.mymail.inboxdoctor.dto.UserDTO;
import com.mymail.inboxdoctor.entity.User;
import com.mymail.inboxdoctor.service.UserService;

import java.security.Principal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@RestController
@RequestMapping(value = "/api")
public class UserController {
	
	@Autowired
	UserService userService;
	
    @RequestMapping(value = "/user/changePassword", method = RequestMethod.PUT)
	public ResponseEntity<?> changeUserPassword(@RequestBody User user, Principal principal){
    	User currentUser = userService.findByUsername(principal.getName());

    	if(!user.getPassword().equals("")) {
	    	String encodedPassword = new BCryptPasswordEncoder().encode(user.getPassword());
	    	currentUser.setPassword(encodedPassword);
    	}else {
    		currentUser.setPassword(currentUser.getPassword());
    	}
    	userService.save(currentUser);

		return ResponseEntity.status(200).build();
	}
    
    @RequestMapping(value = "/user/add", method = RequestMethod.POST)
	public ResponseEntity<UserDTO> addUser(@RequestBody User user){
    	String encodedPassword = new BCryptPasswordEncoder().encode(user.getPassword());
    	user.setPassword(encodedPassword);
	    	if(!exist(user.getUsername())) {
		    	userService.save(user);
		    	User userForAuthority = userService.findByUsername(user.getUsername());
		    	userService.insertAuthority(userForAuthority.getId(), 1);
		    	UserDTO userDTO = new UserDTO(user);
	        return new ResponseEntity<>(userDTO, HttpStatus.OK);
    	}
        return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
	}

	@RequestMapping(value = "/user/get-current", method = RequestMethod.GET)
	public ResponseEntity<UserDTO> getCurrentUser(Principal principal){
		User currentUser = userService.findByUsername(principal.getName());
		UserDTO userDTO = new UserDTO(currentUser);
		return new ResponseEntity<UserDTO>(userDTO, HttpStatus.OK);
	}

	@RequestMapping(value = "/user/update", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<UserDTO> updateUser(@RequestBody User u, Principal principal) {
		User user = userService.findByUsername(principal.getName());
		if(u.getPassword().equals(null)){
			u.setPassword(user.getPassword());
		}
		UserDTO userDTO = new UserDTO(user);
		userService.save(u);
		return new ResponseEntity<>(userDTO, HttpStatus.OK);
	}
    
    public boolean exist(String username) {
    	boolean existing;
    	User user = userService.findByUsername(username);
    	if(user == null) {
    		existing = false;
    	}else {
    		existing = true;
    	}
    	return existing;
    }
}
