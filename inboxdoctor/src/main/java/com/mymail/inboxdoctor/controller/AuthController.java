package com.mymail.inboxdoctor.controller;

import java.io.IOException;

import javax.security.sasl.AuthenticationException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mymail.inboxdoctor.entity.User;
import com.mymail.inboxdoctor.security.AuthenticationRequest;
import com.mymail.inboxdoctor.security.TokenHelper;
import com.mymail.inboxdoctor.security.UserAuthenticationToken;

@RestController
@RequestMapping("/authenticate")
public class AuthController {
	
	@Autowired
	private AuthenticationManager authenticationManager;
	
	@Autowired
	private TokenHelper tokenHelper;
	
	@PostMapping
	public UserAuthenticationToken authenticateUser(@RequestBody AuthenticationRequest tokenRequest) throws AuthenticationException, IOException {
		UsernamePasswordAuthenticationToken authenticationRequest = new UsernamePasswordAuthenticationToken(tokenRequest.getUserName(), tokenRequest.getPassword());
		final Authentication authentication;

		try {
			authentication = authenticationManager.authenticate(authenticationRequest);
		} catch (org.springframework.security.core.AuthenticationException ex) {
			return null;
		}
		
		SecurityContextHolder.getContext().setAuthentication(authentication);
		
		User user = (User) authentication.getPrincipal();
		
		String token = tokenHelper.generateToken(user.getUsername());
		long expiresIn = tokenHelper.getExiprationDate(token);
		
		UserAuthenticationToken token_response = new UserAuthenticationToken(token, expiresIn);
        return token_response;

		//return ResponseEntity.ok(token_response);
	}

}
