package com.mymail.inboxdoctor.controller;

import com.mymail.inboxdoctor.dto.AttachmentDTO;
import com.mymail.inboxdoctor.dto.ContactDTO;
import com.mymail.inboxdoctor.dto.MessageDTO;
import com.mymail.inboxdoctor.lucene.model.AdvancedQuery;
import com.mymail.inboxdoctor.lucene.model.RequiredHighlight;
import com.mymail.inboxdoctor.lucene.model.SearchType;
import com.mymail.inboxdoctor.lucene.model.SimpleQuery;
import com.mymail.inboxdoctor.lucene.search.QueryBuilder;
import com.mymail.inboxdoctor.lucene.search.ResultRetriever;

import io.searchbox.client.JestClient;
import io.searchbox.client.JestClientFactory;
import io.searchbox.client.config.HttpClientConfig;
import io.searchbox.core.Search;
import io.searchbox.core.SearchResult;

import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/search")
public class SearchController {

    private static final Logger log = LoggerFactory.getLogger(SearchController.class);
    
    private static JestClient client;

    static {
		JestClientFactory factory = new JestClientFactory();
		factory.setHttpClientConfig(new HttpClientConfig
				.Builder("http://localhost:9200")
				.multiThreaded(true)
				.build());
		SearchController.client = factory.getObject();
	}
	
    @PostMapping(value="/message/term", consumes="application/json")
    public ResponseEntity<List<MessageDTO>> searchTermQueryMessage(Principal principal, @RequestBody SimpleQuery simpleQuery) throws Exception {
    	log.warn("Created SimpleQuery with field: " + simpleQuery.getField() + " and value: " + simpleQuery.getValue() + " and search type is " + SearchType.REGULAR);
        List<RequiredHighlight> rh = new ArrayList<>();
		org.elasticsearch.index.query.QueryBuilder query= QueryBuilder.buildQuery(SearchType.PHRASE, simpleQuery.getField(), simpleQuery.getValue());

        BoolQueryBuilder boolQueryBuilder = QueryBuilders.boolQuery();
        boolQueryBuilder.must(QueryBuilders.matchQuery(simpleQuery.getField(),simpleQuery.getValue()));
        
        rh.add(new RequiredHighlight(simpleQuery.getField(), simpleQuery.getValue()));
        List<MessageDTO> results = ResultRetriever.getResultsMessage(query, rh);

        List<MessageDTO> filterMessagesDTO = new ArrayList<>();
        for (MessageDTO email : results) {
            if (filterMessagesDTO.stream().filter(m -> m.getId() == email.getId()).findFirst().orElse(null) == null)
                filterMessagesDTO.add(email);
        }
        
        return new ResponseEntity<List<MessageDTO>>(filterMessagesDTO, HttpStatus.OK);
    }
	
	@PostMapping(value="/message/phrase", consumes="application/json")
	public ResponseEntity<List<MessageDTO>> searchPhraseMessage(Principal principal, @RequestBody SimpleQuery simpleQuery) throws Exception {
		org.elasticsearch.index.query.QueryBuilder query= QueryBuilder.buildQuery(SearchType.PHRASE, simpleQuery.getField(), simpleQuery.getValue());
    	log.warn("Created SimpleQuery with field: " + simpleQuery.getField() + " and value: " + simpleQuery.getValue() + " and search type is: " + SearchType.PHRASE);
    	
		List<RequiredHighlight> rh = new ArrayList<RequiredHighlight>();
		rh.add(new RequiredHighlight(simpleQuery.getField(), simpleQuery.getValue()));
		List<MessageDTO> results = ResultRetriever.getResultsMessage(query, rh);

		List<MessageDTO> filterMessagesDTO = new ArrayList<>();
		for (MessageDTO email : results) {
			if (filterMessagesDTO.stream().filter(m -> m.getId() == email.getId()).findFirst().orElse(null) == null)
				filterMessagesDTO.add(email);
		}
		
		return new ResponseEntity<List<MessageDTO>>(filterMessagesDTO, HttpStatus.OK);
	}
	
	@PostMapping(value="/message/nested", consumes="application/json")
	public ResponseEntity<List<MessageDTO>> searchExample(Principal principal, @RequestBody SimpleQuery simpleQuery) throws Exception {
    	log.warn("Created SimpleQuery with field: " + simpleQuery.getField() + " and value: " + simpleQuery.getValue() + " and search type is::::::: " + SearchType.NESTED);
    	List<MessageDTO> results = new ArrayList<>();

    	String e = "{\r\n" +
    			"  \"query\": {\r\n" +
    			"    \"bool\": {\r\n" +
    			"      \"must\": [\r\n" +
    			"        {\r\n" +
    			"          \"match\": {\r\n" +
    			"            \""+simpleQuery.getField()+"\": \"* " + simpleQuery.getValue() + "*\"" +
    			"          }\r\n" +
    			"        }\r\n" +
    			"      ]\r\n" +
    			"    }\r\n" +
    			"  }\r\n" +
    			"}";
    	
    	List<SearchResult.Hit<MessageDTO, Void>> searchResults =
    			client.execute(new Search.Builder(e).build())
    			    .getHits(MessageDTO.class);

        for (SearchResult.Hit<MessageDTO, Void> sd : searchResults) {
			MessageDTO me = new MessageDTO();
			me.setContent(sd.source.getContent());
        	me.setSubject(sd.source.getSubject());
        	me.setDateTime(sd.source.getDateTime());
        	me.setFrom(sd.source.getFrom());
        	me.setAccount(sd.source.getAccount());
            me.setFolder(sd.source.getFolder());
            me.setTags(sd.source.getTags());
            me.setTo(sd.source.getTo());
            results.add(me);
        }

        List<MessageDTO> filterMessagesDTO = new ArrayList<>();
        for (MessageDTO email : results) {
            if (filterMessagesDTO.stream().filter(m -> m.getId() == email.getId()).findFirst().orElse(null) == null)
                filterMessagesDTO.add(email);
        }
        
		return new ResponseEntity<List<MessageDTO>>(filterMessagesDTO, HttpStatus.OK);
	}
	
	@PostMapping(value="/message/search-boolean", consumes="application/json")
	public ResponseEntity<List<MessageDTO>> searchBooleanMessage(Principal principal, @RequestBody AdvancedQuery advancedQuery) throws Exception {
		log.warn("Created regular with field: " + advancedQuery.getField1() + " and value: " + advancedQuery.getValue1() + " and search type is Boolean.");

		BoolQueryBuilder builder = QueryBuilders.boolQuery();
		if(advancedQuery.getOperation().equalsIgnoreCase("AND")){
			builder.must(QueryBuilders.matchQuery(advancedQuery.getField1(),advancedQuery.getValue1()));
			builder.must(QueryBuilders.matchQuery(advancedQuery.getField2(),advancedQuery.getValue2()));
		}else if(advancedQuery.getOperation().equalsIgnoreCase("OR")){
			builder.should(QueryBuilders.matchQuery(advancedQuery.getField1(),advancedQuery.getValue1()));
			builder.should(QueryBuilders.matchQuery(advancedQuery.getField2(),advancedQuery.getValue2()));
		}else if(advancedQuery.getOperation().equalsIgnoreCase("NOT")){
			builder.must(QueryBuilders.matchQuery(advancedQuery.getField1(),advancedQuery.getValue1()));
			builder.mustNot(QueryBuilders.matchQuery(advancedQuery.getField2(),advancedQuery.getValue2()));
		}

		List<RequiredHighlight> rh = new ArrayList<RequiredHighlight>();
		rh.add(new RequiredHighlight(advancedQuery.getField1(), advancedQuery.getValue1()));
		rh.add(new RequiredHighlight(advancedQuery.getField2(), advancedQuery.getValue2()));
		List<MessageDTO> results = ResultRetriever.getResultsMessage(builder, rh);

		List<MessageDTO> filterMessageDTO = new ArrayList<>();
		for (MessageDTO message : results) {
			if (filterMessageDTO.stream().filter(m -> m.getId() == message.getId()).findFirst().orElse(null) == null)
				filterMessageDTO.add(message);
		}
		return new ResponseEntity<List<MessageDTO>>(filterMessageDTO, HttpStatus.OK);
	}
	
    @PostMapping(value="/contact/term", consumes="application/json")
    public ResponseEntity<List<ContactDTO>> searchTermQueryContact(Principal principal, @RequestBody SimpleQuery simpleQuery) throws Exception {
    	
    	log.warn("Created SimpleQuery with field: " + simpleQuery.getField() + " and value: " + simpleQuery.getValue() + " and search type is " + SearchType.REGULAR);
        org.elasticsearch.index.query.QueryBuilder query= QueryBuilder.buildQuery(SearchType.REGULAR, simpleQuery.getField(), simpleQuery.getValue());
        List<RequiredHighlight> rh = new ArrayList<>();

		BoolQueryBuilder boolQueryBuilder = QueryBuilders.boolQuery();
		boolQueryBuilder.must(QueryBuilders.matchQuery(simpleQuery.getField(),simpleQuery.getValue()));

        rh.add(new RequiredHighlight(simpleQuery.getField(), simpleQuery.getValue()));
        List<ContactDTO> results = ResultRetriever.getResultsContact(boolQueryBuilder, rh);
        
        List<ContactDTO> filterContactsDTO = new ArrayList<>();
        for (ContactDTO contact : results) {
            if (filterContactsDTO.stream().filter(m -> m.getId() == contact.getId()).findFirst().orElse(null) == null)
				filterContactsDTO.add(contact);
        }
        
        return new ResponseEntity<List<ContactDTO>>(filterContactsDTO, HttpStatus.OK);
    }

	@PostMapping(value="/contact/phrase", consumes="application/json")
	public ResponseEntity<List<ContactDTO>> searchPhraseContact(Principal principal, @RequestBody SimpleQuery simpleQuery) throws Exception {
		org.elasticsearch.index.query.QueryBuilder query= QueryBuilder.buildQuery(SearchType.PHRASE, simpleQuery.getField(), simpleQuery.getValue());
		log.warn("Created SimpleQuery with field: " + simpleQuery.getField() + " and value: " + simpleQuery.getValue() + " and search type is: " + SearchType.PHRASE);

		List<RequiredHighlight> rh = new ArrayList<RequiredHighlight>();
		rh.add(new RequiredHighlight(simpleQuery.getField(), simpleQuery.getValue()));
		List<ContactDTO> results = ResultRetriever.getResultsContact(query, rh);

		List<ContactDTO> filterContactsDTO = new ArrayList<>();
		for (ContactDTO contact : results) {
			if (filterContactsDTO.stream().filter(m -> m.getId() == contact.getId()).findFirst().orElse(null) == null)
				filterContactsDTO.add(contact);
		}

		return new ResponseEntity<List<ContactDTO>>(filterContactsDTO, HttpStatus.OK);
	}

	@PostMapping(value="/contact/search-boolean", consumes="application/json")
	public ResponseEntity<List<ContactDTO>> searchBooleanContact(Principal principal, @RequestBody AdvancedQuery advancedQuery) throws Exception {
		log.warn("Created regular with field: " + advancedQuery.getField1() + " and value: " + advancedQuery.getValue1() + " and search type is Boolean.");

		BoolQueryBuilder builder = QueryBuilders.boolQuery();
		if(advancedQuery.getOperation().equalsIgnoreCase("AND")){
			builder.must(QueryBuilders.matchQuery(advancedQuery.getField1(),advancedQuery.getValue1()));
			builder.must(QueryBuilders.matchQuery(advancedQuery.getField2(),advancedQuery.getValue2()));
		}else if(advancedQuery.getOperation().equalsIgnoreCase("OR")){
			builder.should(QueryBuilders.matchQuery(advancedQuery.getField1(),advancedQuery.getValue1()));
			builder.should(QueryBuilders.matchQuery(advancedQuery.getField2(),advancedQuery.getValue2()));
		}else if(advancedQuery.getOperation().equalsIgnoreCase("NOT")){
			builder.must(QueryBuilders.matchQuery(advancedQuery.getField1(),advancedQuery.getValue1()));
			builder.mustNot(QueryBuilders.matchQuery(advancedQuery.getField2(),advancedQuery.getValue2()));
		}

		List<RequiredHighlight> rh = new ArrayList<RequiredHighlight>();
		rh.add(new RequiredHighlight(advancedQuery.getField1(), advancedQuery.getValue1()));
		rh.add(new RequiredHighlight(advancedQuery.getField2(), advancedQuery.getValue2()));
		List<ContactDTO> results = ResultRetriever.getResultsContact(builder, rh);

		List<ContactDTO> filterContactsDTO = new ArrayList<>();
		for (ContactDTO contact : results) {
			if (filterContactsDTO.stream().filter(m -> m.getId() == contact.getId()).findFirst().orElse(null) == null)
				filterContactsDTO.add(contact);
		}

		return new ResponseEntity<List<ContactDTO>>(filterContactsDTO, HttpStatus.OK);
	}

	@PostMapping(value="/message/phrase/attach", consumes="application/json")
	public ResponseEntity<List<MessageDTO>> searchPhraseAttach(Principal principal, @RequestBody SimpleQuery simpleQuery) throws Exception {
		org.elasticsearch.index.query.QueryBuilder query= QueryBuilder.buildQuery(SearchType.PHRASE, simpleQuery.getField(), simpleQuery.getValue());
		log.warn("Created SimpleQuery with field: " + simpleQuery.getField() + " and value: " + simpleQuery.getValue() + " and search type is: " + SearchType.PHRASE);

		List<RequiredHighlight> rh = new ArrayList<RequiredHighlight>();
		rh.add(new RequiredHighlight(simpleQuery.getField(), simpleQuery.getValue()));
		List<AttachmentDTO> results = ResultRetriever.getResultsAttachment(query, rh);

		List<MessageDTO> messageDTOS = new ArrayList<>();
		for(AttachmentDTO attachmentDTO: results){
			messageDTOS.add(attachmentDTO.getMessage());
		}

		List<MessageDTO> filterMessagesDTO = new ArrayList<>();
		for (MessageDTO email : messageDTOS) {
			if (filterMessagesDTO.stream().filter(m -> m.getId() == email.getId()).findFirst().orElse(null) == null)
				filterMessagesDTO.add(email);
		}

		return new ResponseEntity<List<MessageDTO>>(filterMessagesDTO, HttpStatus.OK);
	}

	@PostMapping(value="/message/bool/attach", consumes="application/json")
	public ResponseEntity<List<MessageDTO>> searchBoolAttach(Principal principal, @RequestBody AdvancedQuery advancedQuery) throws Exception {
		log.warn("Created regular with field: " + advancedQuery.getField1() + " and value: " + advancedQuery.getValue1() + " and search type is Boolean.");

		BoolQueryBuilder builder = QueryBuilders.boolQuery();
		if(advancedQuery.getOperation().equalsIgnoreCase("AND")){
			builder.must(QueryBuilders.matchQuery(advancedQuery.getField1(),advancedQuery.getValue1()));
			builder.must(QueryBuilders.matchQuery(advancedQuery.getField2(),advancedQuery.getValue2()));
		}else if(advancedQuery.getOperation().equalsIgnoreCase("OR")){
			builder.should(QueryBuilders.matchQuery(advancedQuery.getField1(),advancedQuery.getValue1()));
			builder.should(QueryBuilders.matchQuery(advancedQuery.getField2(),advancedQuery.getValue2()));
		}else if(advancedQuery.getOperation().equalsIgnoreCase("NOT")){
			builder.must(QueryBuilders.matchQuery(advancedQuery.getField1(),advancedQuery.getValue1()));
			builder.mustNot(QueryBuilders.matchQuery(advancedQuery.getField2(),advancedQuery.getValue2()));
		}

		List<RequiredHighlight> rh = new ArrayList<RequiredHighlight>();
		rh.add(new RequiredHighlight(advancedQuery.getField1(), advancedQuery.getValue1()));
		rh.add(new RequiredHighlight(advancedQuery.getField2(), advancedQuery.getValue2()));
		List<AttachmentDTO> results = ResultRetriever.getResultsAttachment(builder, rh);

		List<MessageDTO> messageDTOSS = new ArrayList<>();
		for(AttachmentDTO attachmentDTO: results){
			messageDTOSS.add(attachmentDTO.getMessage());
		}

		List<MessageDTO> filterMessagesDTO = new ArrayList<>();
		for (MessageDTO message : messageDTOSS) {
			if (filterMessagesDTO.stream().filter(m -> m.getId() == message.getId()).findFirst().orElse(null) == null)
				filterMessagesDTO.add(message);
		}
		return new ResponseEntity<List<MessageDTO>>(filterMessagesDTO, HttpStatus.OK);
	}
}
