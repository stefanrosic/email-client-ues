package com.mymail.inboxdoctor.dto;

import com.mymail.inboxdoctor.entity.Contact;
import com.mymail.inboxdoctor.entity.EMessageTextFormat;
import com.mymail.inboxdoctor.entity.Photo;
import com.mymail.inboxdoctor.helper.Helper;

import java.io.Serializable;
import java.util.Set;

@SuppressWarnings("unused")
public class ContactWListsDTO implements Serializable {

    private int id;

    private String firstName;

    private String lastName;

    private String displayName;

    private String email;

    private Photo photo;

    private EMessageTextFormat format;

    private String note;

    private Set<MessageDTO> to;

    private Set<MessageDTO> cc;

    private Set<MessageDTO> bcc;

    private Set<MessageDTO> from;

    public ContactWListsDTO() {
        super();
    }

    public ContactWListsDTO(Contact contact) {
        this.id = contact.getId();
        this.firstName = contact.getFirstName();
        this.lastName = contact.getLastName();
        this.displayName = contact.getDisplayName();
        this.email = contact.getEmail();
        this.photo = contact.getPhoto();
        this.format = contact.getFormat();
        this.note = contact.getNote();
        this.to = Helper.toMessageDTOSet(contact.getTo());
        this.cc = Helper.toMessageDTOSet(contact.getCc());
        this.bcc = Helper.toMessageDTOSet(contact.getBcc());
        this.from = Helper.toMessageDTOSet(contact.getFrom());
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Photo getPhoto() {
        return photo;
    }

    public void setPhoto(Photo photo) {
        this.photo = photo;
    }

    public EMessageTextFormat getFormat() {
        return format;
    }

    public void setFormat(EMessageTextFormat format) {
        this.format = format;
    }

    public Set<MessageDTO> getTo() {
        return to;
    }

    public void setTo(Set<MessageDTO> to) {
        this.to = to;
    }

    public Set<MessageDTO> getCc() {
        return cc;
    }

    public void setCc(Set<MessageDTO> cc) {
        this.cc = cc;
    }

    public Set<MessageDTO> getBcc() {
        return bcc;
    }

    public void setBcc(Set<MessageDTO> bcc) {
        this.bcc = bcc;
    }

    public Set<MessageDTO> getFrom() {
        return from;
    }

    public void setFrom(Set<MessageDTO> from) {
        this.from = from;
    }

    public String getFullName() {
        return this.firstName + " " + this.lastName;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }
}
