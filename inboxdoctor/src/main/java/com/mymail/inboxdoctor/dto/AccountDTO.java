package com.mymail.inboxdoctor.dto;


import com.mymail.inboxdoctor.entity.Account;

import java.io.Serializable;

@SuppressWarnings("unused")
public class AccountDTO implements Serializable {
    private int id;

    private String smtp;

    private String pop3imap;

    private String username;
    
    private String password;

    public AccountDTO() {
    }

    public AccountDTO(Account account) {
        this.id = account.getId();
        this.smtp = account.getSmtp();
        this.pop3imap = account.getPop3imap();
        this.username = account.getUsername();
        this.password = account.getPassword();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSmtp() {
        return smtp;
    }

    public void setSmtp(String smtp) {
        this.smtp = smtp;
    }

    public String getPop3imap() {
        return pop3imap;
    }

    public void setPop3imap(String pop3imap) {
        this.pop3imap = pop3imap;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
    
    
}
