package com.mymail.inboxdoctor.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Set;

import com.mymail.inboxdoctor.entity.Attachment;
import com.mymail.inboxdoctor.entity.Message;
import com.mymail.inboxdoctor.helper.Helper;

@SuppressWarnings("unused")
public class MessageDTO implements Serializable {

    private int id;

    private ContactDTO from;

    private Date dateTime;

    private String subject;

    private String content;

    private FolderDTO folder;

    private AccountDTO account;

    private List<TagDTO> tags;

    private boolean read;

    private Set<ContactDTO> to;

    public MessageDTO() {
    }

    public MessageDTO(Message message) {
        super();
        this.id = message.getId();
        this.from = new ContactDTO(message.getFrom());
        this.dateTime = message.getDateTime();
        this.subject = message.getSubject();
        this.content = message.getContent();
        this.folder = new FolderDTO(message.getFolder());
        this.account = new AccountDTO(message.getAccount());
        this.read = message.isRead();
        this.tags = Helper.toTagsDTOList(message.getTags());
        this.to = Helper.toContactDTOSet(message.getTo());
    }

    public List<TagDTO> getTags() {
        return tags;
    }

    public void setTags(List<TagDTO> tags) {
        this.tags = tags;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public ContactDTO getFrom() {
        return from;
    }

    public void setFrom(ContactDTO from) {
        this.from = from;
    }

    public Date getDateTime() {
        return dateTime;
    }

    public void setDateTime(Date dateTime) {
        this.dateTime = dateTime;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public FolderDTO getFolder() {
        return folder;
    }

    public void setFolder(FolderDTO folder) {
        this.folder = folder;
    }

    public AccountDTO getAccount() {
        return account;
    }

    public void setAccount(AccountDTO account) {
        this.account = account;
    }

    public boolean isRead() {
        return read;
    }

    public void setRead(boolean read) {
        this.read = read;
    }

	public Set<ContactDTO> getTo() {
		return to;
	}

	public void setTo(Set<ContactDTO> to) {
		this.to = to;
	}

}