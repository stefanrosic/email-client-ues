package com.mymail.inboxdoctor.dto;

import com.mymail.inboxdoctor.entity.Attachment;

public class AttachmentDTO {

    private int id;

    private MessageDTO message;

    private String content;

    private String type;

    private String name;

    public AttachmentDTO(){}

    public AttachmentDTO(Attachment attachment) {
        this.id = attachment.getId();
        this.message = new MessageDTO(attachment.getMessage());
        this.content = attachment.getBase64();
        this.type = attachment.getType();
        this.name = attachment.getName();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public MessageDTO getMessage() {
        return message;
    }

    public void setMessage(MessageDTO message) {
        this.message = message;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
