package com.mymail.inboxdoctor.dto;

import com.mymail.inboxdoctor.entity.Folder;
import com.mymail.inboxdoctor.entity.Rule;
import com.mymail.inboxdoctor.helper.Helper;

import java.io.Serializable;
import java.util.Set;

@SuppressWarnings("unused")
public class FolderWListsDTO implements Serializable {

    private int id;

    private String name;

    private Set<FolderWListsDTO> subFolders;

    private Rule destination;

    private Set<MessageWListsDTO> messages;

    public FolderWListsDTO() {
        super();
    }

    public FolderWListsDTO(Folder folder) {
        this.id = folder.getId();
        this.name = folder.getName();
        this.subFolders = Helper.toFolderWListsDTOSet(folder.getSubFolders());
        this.messages = Helper.toMessageWListsDTOSet(folder.getMessages());
        this.destination = folder.getDestination();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<FolderWListsDTO> getSubFolders() {
        return subFolders;
    }

    public void setSubFolders(Set<FolderWListsDTO> subFolders) {
        this.subFolders = subFolders;
    }

    public Rule getDestination() {
        return destination;
    }

    public void setDestination(Rule destination) {
        this.destination = destination;
    }

    public Set<MessageWListsDTO> getMessages() {
        return messages;
    }

    public void setMessages(Set<MessageWListsDTO> messages) {
        this.messages = messages;
    }
}
