package com.mymail.inboxdoctor.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Set;

import com.mymail.inboxdoctor.entity.*;
import com.mymail.inboxdoctor.helper.Helper;

@SuppressWarnings("unused")
public class MessageWListsDTO implements Serializable {

    private int id;

    private ContactDTO from;

    private Date dateTime;

    private String subject;

    private String content;

    private FolderDTO folder;

    private AccountDTO account;

    private Set<ContactDTO> to;

    private Set<ContactDTO> cc;

    private Set<ContactDTO> bcc;

    private List<AttachmentsDTO> attachments;

    private List<TagDTO> tags;

    private boolean read;

    public MessageWListsDTO() {
    }

    public MessageWListsDTO(Message message) {
        super();
        this.id = message.getId();
        this.from = new ContactDTO(message.getFrom());
        this.dateTime = message.getDateTime();
        this.subject = message.getSubject();
        this.content = message.getContent();
        this.folder = new FolderDTO(message.getFolder());
        this.account = new AccountDTO(message.getAccount());
        this.to = Helper.toContactDTOSet(message.getTo());
        this.cc = Helper.toContactDTOSet(message.getCc());
        this.bcc = Helper.toContactDTOSet(message.getBcc());
        this.read = message.isRead();
        this.tags = Helper.toTagsDTOList(message.getTags());
        this.attachments = Helper.toAttachmentsDTOList(message.getAttachments());
    }

    public List<TagDTO> getTags() {
        return tags;
    }

    public void setTags(List<TagDTO> tags) {
        this.tags = tags;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public ContactDTO getFrom() {
        return from;
    }

    public void setFrom(ContactDTO from) {
        this.from = from;
    }

    public Date getDateTime() {
        return dateTime;
    }

    public void setDateTime(Date dateTime) {
        this.dateTime = dateTime;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public FolderDTO getFolder() {
        return folder;
    }

    public void setFolder(FolderDTO folder) {
        this.folder = folder;
    }

    public AccountDTO getAccount() {
        return account;
    }

    public void setAccount(AccountDTO account) {
        this.account = account;
    }

    public Set<ContactDTO> getTo() {
        return to;
    }

    public void setTo(Set<ContactDTO> to) {
        this.to = to;
    }

    public Set<ContactDTO> getCc() {
        return cc;
    }

    public void setCc(Set<ContactDTO> cc) {
        this.cc = cc;
    }

    public Set<ContactDTO> getBcc() {
        return bcc;
    }

    public void setBcc(Set<ContactDTO> bcc) {
        this.bcc = bcc;
    }

    public boolean isRead() {
        return read;
    }

    public void setRead(boolean read) {
        this.read = read;
    }

    public List<AttachmentsDTO> getAttachments() {
        return attachments;
    }

    public void setAttachments(List<AttachmentsDTO> attachments) {
        this.attachments = attachments;
    }
}
