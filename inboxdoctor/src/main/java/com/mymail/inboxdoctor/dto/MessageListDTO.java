package com.mymail.inboxdoctor.dto;

import com.mymail.inboxdoctor.entity.Message;

import java.io.Serializable;
import java.util.Date;

@SuppressWarnings("unused")
public class MessageListDTO implements Serializable {

    private int id;

    private Date dateTime;

    private String subject;

    private String content;

    private ContactDTO from;

    private boolean read;

    public MessageListDTO() {
    }

    public MessageListDTO(Message message) {
        this.id = message.getId();
        this.dateTime = message.getDateTime();
        this.subject = message.getSubject();
        this.content = message.getContent();
        this.from = new ContactDTO(message.getFrom());
        this.read = message.isRead();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getDateTime() {
        return dateTime;
    }

    public void setDateTime(Date dateTime) {
        this.dateTime = dateTime;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public boolean isRead() {
        return read;
    }

    public ContactDTO getFrom() {
        return from;
    }

    public void setFrom(ContactDTO from) {
        this.from = from;
    }

    public void setRead(boolean read) {
        this.read = read;
    }
}
