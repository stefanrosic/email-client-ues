package com.mymail.inboxdoctor.dto;

import com.mymail.inboxdoctor.entity.Folder;
import com.mymail.inboxdoctor.entity.Rule;

import java.io.Serializable;

@SuppressWarnings("unused")
public class FolderDTO implements Serializable {

    private int id;

    private String name;

    private Rule destination;

    public FolderDTO() {
        super();
    }

    public FolderDTO(Folder folder) {
        this.id = folder.getId();
        this.name = folder.getName();
        this.destination = folder.getDestination();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Rule getDestination() {
        return destination;
    }

    public void setDestination(Rule destination) {
        this.destination = destination;
    }
}
