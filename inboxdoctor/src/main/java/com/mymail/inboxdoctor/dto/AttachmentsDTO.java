package com.mymail.inboxdoctor.dto;

import com.mymail.inboxdoctor.entity.Attachment;

@SuppressWarnings("unused")
public class AttachmentsDTO {

    private int id;

    private int messageId;

    private String base64;

    private String type;

    private String name;

    public AttachmentsDTO(){}

    public AttachmentsDTO(Attachment attachment) {
        this.id = attachment.getId();
        this.base64 = attachment.getBase64();
        this.type = attachment.getType();
        this.name = attachment.getName();
        this.messageId = attachment.getMessage().getId();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getBase64() {
        return base64;
    }

    public void setBase64(String base64) {
        this.base64 = base64;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getMessageId() {
        return messageId;
    }

    public void setMessageId(int messageId) {
        this.messageId = messageId;
    }
}
