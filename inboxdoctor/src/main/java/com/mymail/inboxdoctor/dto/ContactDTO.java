package com.mymail.inboxdoctor.dto;

import com.mymail.inboxdoctor.entity.Contact;
import com.mymail.inboxdoctor.entity.EMessageTextFormat;
import com.mymail.inboxdoctor.entity.Photo;

import java.io.Serializable;

@SuppressWarnings("unused")
public class ContactDTO implements Serializable {

    private int id;

    private String firstName;

    private String lastName;

    private String displayName;

    private String email;

    private String note;

    private Photo photo;

    private EMessageTextFormat format;

    public ContactDTO() {
        super();
    }

    public ContactDTO(Contact contact) {
        this.id = contact.getId();
        this.firstName = contact.getFirstName();
        this.lastName = contact.getLastName();
        this.displayName = contact.getDisplayName();
        this.email = contact.getEmail();
        this.photo = contact.getPhoto();
        this.format = contact.getFormat();
        this.note = contact.getNote();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Photo getPhoto() {
        return photo;
    }

    public void setPhoto(Photo photo) {
        this.photo = photo;
    }

    public EMessageTextFormat getFormat() {
        return format;
    }

    public void setFormat(EMessageTextFormat format) {
        this.format = format;
    }

    public String getFullName() {
        return this.firstName + " " + this.lastName;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }
}
