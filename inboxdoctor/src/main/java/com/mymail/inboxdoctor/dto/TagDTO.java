package com.mymail.inboxdoctor.dto;

import com.mymail.inboxdoctor.entity.Tag;

@SuppressWarnings("unused")
public class TagDTO {

    private int id;
    private String name;

    public TagDTO() {
    }

    public TagDTO(Tag tag) {
        this.id = tag.getId();
        this.name = tag.getName();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
