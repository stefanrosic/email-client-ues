package com.mymail.inboxdoctor.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@SuppressWarnings("unused")
@Entity
@Table(name = "folders")
public class Folder implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "folder_id", unique = true, nullable = false)
    private int id;

    @Column(name = "name", nullable = false)
    private String name;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER, mappedBy = "parentFolder")
    private Set<Folder> subFolders;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "parentFolder", referencedColumnName = "folder_id")
    private Folder parentFolder;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "account_id", referencedColumnName = "account_id")
    private Account account;

    @ManyToOne
    @JoinColumn(name = "destination_id", referencedColumnName = "rule_id", nullable = false)
    private Rule destination;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER, mappedBy = "folder")
    private Set<Message> messages;

    public Folder() {
        super();
    }

    public Folder(int id, String name, Set<Folder> subFolders, Set<Message> messages, Rule destination, Account account) {
        this.id = id;
        this.name = name;
        this.subFolders = subFolders;
        this.messages = messages;
        this.destination = destination;
        this.account = account;
    }

    public Folder(String name, Rule rule, Account account) {
        this.name = name;
        this.subFolders = new HashSet<>();
        this.messages = new HashSet<>();
        this.destination = rule;
        this.account = account;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<Message> getMessages() {
        return messages;
    }

    public void setMessages(Set<Message> messages) {
        this.messages = messages;
    }

    public void setChildFolder(Set<Folder> subFolders) {
        this.subFolders = subFolders;
    }

    public Set<Folder> getSubFolders() {
        return subFolders;
    }

    public Rule getDestination() {
        return destination;
    }

    public void setDestination(Rule destination) {
        this.destination = destination;
    }

    public void setSubFolders(Set<Folder> subFolders) {
        this.subFolders = subFolders;
    }

    public Folder getParentFolder() {
        return parentFolder;
    }

    public void setParentFolder(Folder parentFolder) {
        this.parentFolder = parentFolder;
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    @Override
	public String toString() {
		return "Folder [id=" + id + ", name=" + name + ", subFolders=" + subFolders + ", parentFolder=" + parentFolder
				+ ", destination=" + destination + ", messages=" + messages + "]";
	}
    
    
}
