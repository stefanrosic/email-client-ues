package com.mymail.inboxdoctor.entity;

import javax.persistence.*;

import org.springframework.data.elasticsearch.annotations.Document;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Set;

@SuppressWarnings("unused")
@Entity
@Table(name = "messages")
public class Message implements Serializable {

    private static SimpleDateFormat df = new SimpleDateFormat("dd.MM.yyyy.");
    private static SimpleDateFormat dfFull = new SimpleDateFormat("dd.MM.yyyy. (HH:mm)");

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "message_id", unique = true, nullable = false)
    private int id;

    @ManyToOne
    @JoinColumn(name = "_from", referencedColumnName = "contact_id", nullable = false)
    private Contact from;

    @ManyToMany(fetch = FetchType.EAGER)
    private Set<Contact> to;

    @ManyToMany(fetch = FetchType.EAGER)
    private Set<Contact> cc;

    @ManyToMany(fetch = FetchType.EAGER)
    private Set<Contact> bcc;

    @Column(name = "date_time", nullable = false)
    private Date dateTime;

    @Column(name = "subject", nullable = false)
    private String subject;

    @Column(name = "content", nullable = false, columnDefinition = "LONGTEXT")
    private String content;

    @ManyToMany(fetch = FetchType.EAGER)
    private Set<Tag> tags;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER, mappedBy = "message")
    private Set<Attachment> attachments;

    @ManyToOne
    @JoinColumn(name = "folder", referencedColumnName = "folder_id", nullable = false)
    private Folder folder;

    @ManyToOne
    @JoinColumn(name = "account", referencedColumnName = "account_id", nullable = false)
    private Account account;

    @Column(name = "_read", nullable = false)
    private boolean read;

    public Message() {
    }

    public Message(int id, Contact from, Set<Contact> to, Set<Contact> cc, Set<Contact> bcc,
                   Date dateTime, String subject, String content, Set<Tag> tags,
                   Set<Attachment> attachments, Folder folder, Account account, boolean read) {
        this.id = id;
        this.from = from;
        this.to = to;
        this.cc = cc;
        this.bcc = bcc;
        this.dateTime = dateTime;
        this.subject = subject;
        this.content = content;
        this.tags = tags;
        this.attachments = attachments;
        this.folder = folder;
        this.account = account;
        this.read = read;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Contact getFrom() {
        return from;
    }

    public void setFrom(Contact from) {
        this.from = from;
    }

    public Set<Contact> getTo() {
        return to;
    }

    public void setTo(Set<Contact> to) {
        this.to = to;
    }

    public Set<Contact> getCc() {
        return cc;
    }

    public void setCc(Set<Contact> cc) {
        this.cc = cc;
    }

    public Set<Contact> getBcc() {
        return bcc;
    }

    public void setBcc(Set<Contact> bcc) {
        this.bcc = bcc;
    }

    public Date getDateTime() {
        return dateTime;
    }

    public void setDateTime(Date dateTime) {
        this.dateTime = dateTime;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Set<Tag> getTags() {
        return tags;
    }

    public void setTags(Set<Tag> tags) {
        this.tags = tags;
    }

    public Set<Attachment> getAttachments() {
        return attachments;
    }

    public void setAttachments(Set<Attachment> attachments) {
        this.attachments = attachments;
    }

    public Folder getFolder() {
        return folder;
    }

    public void setFolder(Folder folder) {
        this.folder = folder;
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }


    public boolean isRead() {
        return read;
    }

    public void setRead(boolean read) {
        this.read = read;
    }

    @Override
    public String toString() {
        return subject;
    }
    
    public String stringMethod() {
    	return "Message [id=" + id + ", from=" + from + ", to=" + to + ", cc=" + cc + ", bcc=" + bcc + ", dateTime="
				+ dateTime + ", subject=" + subject + ", content=" + content + ", tags=" + tags + ", attachments="
				+ attachments + ", folder=" + folder + ", account=" + account + ", read=" + read + "]";
    }
    

    public String buildTags() {
        StringBuilder builderString = new StringBuilder();
        for (Tag tag : this.tags) {
            builderString.append(tag.getName());
            builderString.append(",");
        }

        return builderString + "";
    }

    public String buildCC() {
        StringBuilder builderString = new StringBuilder();
        for (Contact cc : this.cc) {
            builderString.append(cc.getEmail());
            builderString.append(",");
        }

        return builderString + "";
    }

    public String buildBCC() {
        StringBuilder builderString = new StringBuilder();
        for (Contact bcc : this.bcc) {
            builderString.append(bcc.getEmail());
            builderString.append(",");
        }

        return builderString + "";
    }

    public String buildTo() {
        StringBuilder builderString = new StringBuilder();
        for (Contact to : this.to) {
            builderString.append(to.getEmail());
            builderString.append(",");
        }

        return builderString + "";
    }

    public String buildAttachment() {
        StringBuilder builderString = new StringBuilder();
        for (Attachment att : this.attachments) {
            builderString.append(att.getName());
            builderString.append(",");
        }

        return builderString + "";
    }

    public String getFormatedDate() {
        return df.format(this.dateTime);
    }

    public String getFormatedFullDate() {
        return dfFull.format(this.dateTime);
    }
    
    
}
