package com.mymail.inboxdoctor.entity;

@SuppressWarnings("unused")
public enum ECondition {
    TO, FROM, CC, SUBJECT
}
