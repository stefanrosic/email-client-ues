package com.mymail.inboxdoctor.entity;

import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.searchbox.annotations.JestId;

public class Jedinica {
	
	@JsonProperty("naziv")
	private String naziv;
	
	@JestId
	private int broj;
	
	public Jedinica(String naziv, int broj) {
		super();
		this.naziv = naziv;
		this.broj = broj;
	}
	public String getNaziv() {
		return naziv;
	}
	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}
	public int getBroj() {
		return broj;
	}
	public void setBroj(int broj) {
		this.broj = broj;
	}
	
	

}
