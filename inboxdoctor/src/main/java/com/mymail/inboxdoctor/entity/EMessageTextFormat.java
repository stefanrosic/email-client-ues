package com.mymail.inboxdoctor.entity;

@SuppressWarnings("unused")
public enum EMessageTextFormat {
    PLAIN, HTML
}
