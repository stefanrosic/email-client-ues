package com.mymail.inboxdoctor.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;

@SuppressWarnings("unused")
@Entity
@Table(name = "accounts")
public class Account implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "account_id", unique = true, nullable = false)
    private int id;

    @Column(name = "smtp", nullable = false)
    private String smtp;

    @Column(name = "pop3imap", nullable = false)
    private String pop3imap;

    @Column(name = "username", nullable = false)
    private String username;

    @Column(name = "password", nullable = false)
    private String password;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "account")
    private Set<Message> messages;
    
	@ManyToOne
	@JoinColumn(name = "_user", referencedColumnName = "user_id", nullable = false)
	private User user;


    public Account() {
    }

    public Account(int id, String smtp, String pop3imap, String username, String password, Set<Message> messages, Folder draftFolder
    		,Folder sentFolder,Folder inboxFolder) {
        this.id = id;
        this.smtp = smtp;
        this.pop3imap = pop3imap;
        this.username = username;
        this.password = password;
        this.messages = messages;
    }

    public Account(int id, String smtp, String pop3imap, String username, String password) {
        this.id = id;
        this.smtp = smtp;
        this.pop3imap = pop3imap;
        this.username = username;
        this.password = password;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSmtp() {
        return smtp;
    }

    public void setSmtp(String smtp) {
        this.smtp = smtp;
    }

    public String getPop3imap() {
        return pop3imap;
    }

    public void setPop3imap(String pop3imap) {
        this.pop3imap = pop3imap;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Set<Message> getMessages() {
        return messages;
    }

    public void setMessages(Set<Message> messages) {
        this.messages = messages;
    }

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
	
    
    
}
