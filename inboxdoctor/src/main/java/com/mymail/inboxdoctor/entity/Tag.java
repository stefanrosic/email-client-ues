package com.mymail.inboxdoctor.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;

@SuppressWarnings("unused")
@Entity
@Table(name = "tags")
public class Tag implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "tag_id", unique = true, nullable = false)
    private int id;

    @Column(name = "name", nullable = false)
    private String name;

    @ManyToMany
    private Set<Message> messages;
    
	@ManyToOne
	@JoinColumn(name = "_user", referencedColumnName = "user_id", nullable = false)
	private User user;

    public Tag() {
    }

    public Tag(int id, String name, Set<Message> messages, User user) {
        this.id = id;
        this.name = name;
        this.messages = messages;
        this.user = user;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<Message> getMessages() {
        return messages;
    }

    public void setMessages(Set<Message> messages) {
        this.messages = messages;
    }

    public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	@Override
    public String toString() {
        return this.name;
    }
}
