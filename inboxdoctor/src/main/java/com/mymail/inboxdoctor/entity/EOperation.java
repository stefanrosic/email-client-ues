package com.mymail.inboxdoctor.entity;

@SuppressWarnings("unused")
public enum EOperation {
    MOVE, COPY, DELETE
}
