package com.mymail.inboxdoctor.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;

@SuppressWarnings({"serial", "unused"})
@Entity
@Table(name = "contacts")
public class Contact implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "contact_id", unique = true, nullable = false)
    private int id;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    @Column(name = "display_name")
    private String displayName;

    @Column(name = "email", nullable = false)
    private String email;

    @Column(name = "note")
    private String note;

    @OneToOne
    @JoinColumn(name = "photo_id", referencedColumnName = "photo_id")
    private Photo photo;

    @Column(name = "format")
    private EMessageTextFormat format;

    @ManyToMany(fetch = FetchType.EAGER)
    private Set<Message> to;

    @ManyToMany(fetch = FetchType.EAGER)
    private Set<Message> cc;

    @ManyToMany(fetch = FetchType.EAGER)
    private Set<Message> bcc;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER, mappedBy = "from")
    private Set<Message> from;
    
	@ManyToOne
	@JoinColumn(name = "_user", referencedColumnName = "user_id")
	private User user;
    
    public Contact() {
        super();
    }

    public Contact(int id, String firstName, String lastName, String displayName, String email, Photo photo,
                   EMessageTextFormat format, Set<Message> to, Set<Message> cc, Set<Message> bcc, Set<Message> from, User user) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.displayName = displayName;
        this.email = email;
        this.photo = photo;
        this.format = format;
        this.to = to;
        this.cc = cc;
        this.bcc = bcc;
        this.from = from;
        this.user = user;
    }

    public Contact(String email) {
        this.email = email;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Photo getPhoto() {
        return photo;
    }

    public void setPhoto(Photo photo) {
        this.photo = photo;
    }

    public EMessageTextFormat getFormat() {
        return format;
    }

    public void setFormat(EMessageTextFormat format) {
        this.format = format;
    }

    public Set<Message> getTo() {
        return to;
    }

    public void setTo(Set<Message> to) {
        this.to = to;
    }

    public Set<Message> getCc() {
        return cc;
    }

    public void setCc(Set<Message> cc) {
        this.cc = cc;
    }

    public Set<Message> getBcc() {
        return bcc;
    }

    public void setBcc(Set<Message> bcc) {
        this.bcc = bcc;
    }

    public Set<Message> getFrom() {
        return from;
    }

    public void setFrom(Set<Message> from) {
        this.from = from;
    }
    
    public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    @Override
    public String toString() {
		if(firstName != null && lastName != null) {
        return
                " First name " + firstName.toUpperCase() +
                        ", Last name " + lastName.toUpperCase();
        }
		return "";
    }


    public String buildCC() {
        StringBuilder builderString = new StringBuilder();
        for (Message cc : this.cc) {
            builderString.append(cc);
            builderString.append(",");
        }

        return builderString + "";
    }

    public String buildBCC() {
        StringBuilder builderString = new StringBuilder();
        for (Message bcc : this.bcc) {
            builderString.append(bcc);
            builderString.append(",");
        }

        return builderString + "";
    }

    public String buildTo() {
        StringBuilder builderString = new StringBuilder();
        for (Message to : this.to) {
            builderString.append(to);
            builderString.append(",");
        }

        return builderString + "";
    }

    public String getFullName() {
        return this.firstName + " " + this.lastName;
    }
}
