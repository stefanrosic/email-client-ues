package com.mymail.inboxdoctor.entity;

import javax.persistence.*;
import java.io.Serializable;

@SuppressWarnings("unused")
@Entity
@Table(name = "rules")
public class Rule implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "rule_id", unique = true, nullable = false)
    private int id;

    @Column(name = "_condition", nullable = false)
    private ECondition condition;

    @Column(name = "_operation", nullable = false)
    private EOperation operation;

    public Rule(int id, ECondition condition, EOperation operation) {
        this.id = id;
        this.condition = condition;
        this.operation = operation;
    }

    public Rule(ECondition condition, EOperation operation) {
        this.condition = condition;
        this.operation = operation;
    }

    public Rule() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public ECondition getCondition() {
        return condition;
    }

    public void setCondition(ECondition condition) {
        this.condition = condition;
    }

    public EOperation getOperation() {
        return operation;
    }

    public void setOperation(EOperation operation) {
        this.operation = operation;
    }

	@Override
	public String toString() {
		return "Rule [id=" + id + ", condition=" + condition + ", operation=" + operation + "]";
	}
    
    
}
