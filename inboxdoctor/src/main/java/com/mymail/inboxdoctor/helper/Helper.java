package com.mymail.inboxdoctor.helper;

import com.mymail.inboxdoctor.dto.*;
import com.mymail.inboxdoctor.entity.*;

import javax.mail.Address;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Helper {

    public static Set<ContactDTO> toContactDTOSet(Set<Contact> contacts) {
        Set<ContactDTO> retVal = new HashSet<>();
        for (Contact contact : contacts
        ) {
            retVal.add(new ContactDTO(contact));
        }
        return retVal;
    }

    public static Set<MessageDTO> toMessageDTOSet(Set<Message> messages) {
        Set<MessageDTO> retVal = new HashSet<>();
        for (Message message : messages
        ) {
            retVal.add(new MessageDTO(message));
        }
        return retVal;
    }

    public static Set<MessageWListsDTO> toMessageWListsDTOSet(Set<Message> messages) {
        Set<MessageWListsDTO> retVal = new HashSet<>();
        for (Message message : messages
        ) {
            retVal.add(new MessageWListsDTO(message));
        }
        return retVal;
    }

    public static Set<FolderWListsDTO> toFolderWListsDTOSet(Set<Folder> folders) {
        Set<FolderWListsDTO> retVal = new HashSet<>();
        for (Folder folder : folders
        ) {
            retVal.add(new FolderWListsDTO(folder));
        }
        return retVal;
    }

    public static Set<Contact> fromAddressestoContacts(Address[] addresses) {
        if (addresses == null || addresses.length == 0)
            return new HashSet<>();
        Set<Contact> contacts = new HashSet<>();
        for (Address address : addresses
        ) {
            contacts.add(new Contact(address.toString()));
        }
        return contacts;
    }

    public static List<TagDTO> toTagsDTOList(Set<Tag> tags) {
        List<TagDTO> tagsDTO = new ArrayList<>();
        for (Tag tag : tags
        ) {
            tagsDTO.add(new TagDTO(tag));
        }
        return tagsDTO;
    }

    public static List<AttachmentsDTO> toAttachmentsDTOList(Set<Attachment> attachments) {
        List<AttachmentsDTO> attachmentsDTOS = new ArrayList<>();
        for (Attachment attachment : attachments
        ) {
            attachmentsDTOS.add(new AttachmentsDTO(attachment));
        }
        return attachmentsDTOS;
    }
}
