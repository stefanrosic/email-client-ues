package com.mymail.inboxdoctor.service;

import com.mymail.inboxdoctor.entity.Rule;
import com.mymail.inboxdoctor.repository.RuleRepository;
import org.springframework.stereotype.Service;

@Service
public class RuleService implements RuleServiceInterface{

    private RuleRepository ruleRepository;

    public RuleService(RuleRepository ruleRepository) {
        this.ruleRepository = ruleRepository;
    }

    @Override
    public Rule findOne(Integer ruleId) {
        return ruleRepository.findById(ruleId).isPresent() ? ruleRepository.findById(ruleId).get() : null;
    }
}
