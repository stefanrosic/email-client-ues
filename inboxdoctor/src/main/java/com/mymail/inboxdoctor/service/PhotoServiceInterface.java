package com.mymail.inboxdoctor.service;

import com.mymail.inboxdoctor.entity.Photo;

import java.util.List;

public interface PhotoServiceInterface {

    Photo findOne(Integer photoId);

    List<Photo> findAll();

    Photo save(Photo photo);

    void remove(int photoId);
}
