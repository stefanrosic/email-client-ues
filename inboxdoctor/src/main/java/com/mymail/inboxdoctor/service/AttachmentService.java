package com.mymail.inboxdoctor.service;

import com.mymail.inboxdoctor.entity.Attachment;
import com.mymail.inboxdoctor.repository.AttachmentRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AttachmentService implements AttachmentServiceInterface {

    private AttachmentRepository attachmentRepository;

    public AttachmentService(AttachmentRepository attachmentRepository) {
        this.attachmentRepository = attachmentRepository;
    }

    @Override
    public Attachment findOne(Integer attachmentId) {
        return attachmentRepository.findById(attachmentId).isPresent() ? attachmentRepository.findById(attachmentId).get() : null;
    }

    @Override
    public List<Attachment> findAll() {
        return attachmentRepository.findAll();
    }

    @Override
    public Attachment save(Attachment attachment) {
        return attachmentRepository.save(attachment);
    }

    @Override
    public void remove(int attachmentId) {
        attachmentRepository.deleteById(attachmentId);
    }
}
