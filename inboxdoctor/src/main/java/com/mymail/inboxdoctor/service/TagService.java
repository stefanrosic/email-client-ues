package com.mymail.inboxdoctor.service;

import com.mymail.inboxdoctor.entity.Tag;
import com.mymail.inboxdoctor.repository.TagRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TagService implements TagServiceInterface {

    private TagRepository tagRepository;

    public TagService(TagRepository tagRepository) {
        this.tagRepository = tagRepository;
    }

    @Override
    public Tag findOne(Integer tagId) {
        return tagRepository.findById(tagId).isPresent() ? tagRepository.findById(tagId).get() : null;
    }

    @Override
    public List<Tag> findAll() {
        return tagRepository.findAll();
    }

    @Override
    public Tag save(Tag tag) {
        return tagRepository.save(tag);
    }

    @Override
    public void remove(int tagId) {
        tagRepository.deleteById(tagId);
    }
}
