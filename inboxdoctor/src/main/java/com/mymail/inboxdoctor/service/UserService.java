package com.mymail.inboxdoctor.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.mymail.inboxdoctor.entity.User;
import com.mymail.inboxdoctor.repository.UserRepository;

@Service
public class UserService implements UserServiceInterface{

	private final UserRepository userRepository;
	private static final Logger log = LoggerFactory.getLogger(UserService.class);

	public UserService(UserRepository userRepository) {
		this.userRepository = userRepository;
	}

	@Override
	public User findOne(Integer id) {
		return userRepository.getOne(id);
	}
	
	@Override
	public User save(User user) {
		return userRepository.save(user);
	}

	@Override
	public User findByUsername(String username) {
		return userRepository.findByUsername(username);
	}
	
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException{
		User user = userRepository.findByUsername(username);
		
		if (user == null) {
			log.error("Bad credentials for username");
		} else if (user.getAuthorities().size() == 0) {
			log.error("Unathorized user");
		}
		
		return user;
	}

	@Override
	public void insertAuthority(int user_id, int auth_id) {
		userRepository.insertAuthority(user_id, auth_id);
	}

}
