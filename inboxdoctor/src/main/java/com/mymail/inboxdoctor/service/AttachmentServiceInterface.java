package com.mymail.inboxdoctor.service;

import com.mymail.inboxdoctor.entity.Attachment;

import java.util.List;

public interface AttachmentServiceInterface {

    Attachment findOne(Integer attachmentId);

    List<Attachment> findAll();

    Attachment save(Attachment attachment);

    void remove(int attachmentId);
}
