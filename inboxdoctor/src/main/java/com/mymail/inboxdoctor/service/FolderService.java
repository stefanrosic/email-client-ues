package com.mymail.inboxdoctor.service;

import com.mymail.inboxdoctor.entity.Folder;
import com.mymail.inboxdoctor.repository.FolderRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FolderService implements FolderServiceInterface {

    private FolderRepository folderRepository;

    public FolderService(FolderRepository folderRepository) {
        this.folderRepository = folderRepository;
    }

    @Override
    public Folder findOne(Integer folderId) {
        return folderRepository.findById(folderId).isPresent() ? folderRepository.findById(folderId).get() : null;
    }

    @Override
    public List<Folder> findAll() {
        return folderRepository.findAll();
    }

    @Override
    public Folder save(Folder folder) {
        return folderRepository.save(folder);
    }

    @Override
    public void remove(int folderId) {
        folderRepository.deleteById(folderId);
    }

    @Override
    public Folder findInboxFolderByAccount(int accountId) {
        return folderRepository.findInboxFolderByAccount(accountId);
    }

    @Override
    public Folder findSentFolderByAccount(int accountId) {
        return folderRepository.findSentFolderByAccount(accountId);
    }

    @Override
    public Folder findDraftFolderByAccount(int accountId) {
        return folderRepository.findDraftFolderByAccount(accountId);
    }

    @Override
    public List<Folder> findFoldersByAccount_Id(int accountId) {
        return folderRepository.findFoldersByAccount_Id(accountId);
    }
}
