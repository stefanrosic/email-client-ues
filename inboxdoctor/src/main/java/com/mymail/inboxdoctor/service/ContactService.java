package com.mymail.inboxdoctor.service;

import com.mymail.inboxdoctor.entity.Contact;
import com.mymail.inboxdoctor.entity.User;
import com.mymail.inboxdoctor.lucene.indexer.Indexer;
import com.mymail.inboxdoctor.repository.ContactRepository;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.List;

@Service
public class ContactService implements ContactServiceInterface {
	
    private ContactRepository contactRepository;

    @Override
    public List<Contact> findAllByUserId(int userId) {
        return contactRepository.findAllByUserId(userId);
    }

    public ContactService(ContactRepository contactRepository) {
        this.contactRepository = contactRepository;
    }

    @Override
    public Contact findOne(Integer contactId) {
        return contactRepository.findById(contactId).isPresent() ? contactRepository.findById(contactId).get() : null;
    }

    @Override
    public List<Contact> findAll() {
        return contactRepository.findAll();
    }

    @Override
    public Contact save(Contact contact) {
        return contactRepository.save(contact);
    }

    @Override
    public void remove(int contactId) {
        contactRepository.deleteById(contactId);
    }

	@Override
	public List<Contact> findAllContacts(User user){
		return contactRepository.findAllByUser(user);
	}
}
