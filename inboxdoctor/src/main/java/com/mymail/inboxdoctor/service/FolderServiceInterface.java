package com.mymail.inboxdoctor.service;

import com.mymail.inboxdoctor.entity.Folder;

import java.util.List;

public interface FolderServiceInterface {

    Folder findOne(Integer folderId);

    List<Folder> findAll();

    Folder save(Folder folder);

    void remove(int folderId);

    Folder findInboxFolderByAccount(int accountId);

    Folder findSentFolderByAccount(int accountId);

    Folder findDraftFolderByAccount(int accountId);

    List<Folder> findFoldersByAccount_Id(int accountId);

}
