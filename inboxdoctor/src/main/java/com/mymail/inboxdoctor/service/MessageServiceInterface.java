package com.mymail.inboxdoctor.service;

import java.util.Date;
import java.util.List;

import com.mymail.inboxdoctor.entity.Message;

public interface MessageServiceInterface {

    Message findOne(Integer messageId);

    List<Message> findAll();

    List<Message> findAllByAccountIdAndFolderId(int accountId, int folderId);

    void removeMessagesByAccount(int accountId);

    Message save(Message message);

    void remove(int messageId);

    List<Message> findUnreadMessages(int accountId);

    void setRead(int messageID);

    List<Message> findMessagesByPassedValue(String search_value, int acc);
    
    List<Message> findMessagesByPassedValueSubject(String search_value, int acc);

    List<Message> findMessageWithEmailInBCC(String search_value, int acc);

    List<Message> findMessageWithEmailInCC(String search_value, int acc);

    List<Message> findMessageWithEmailInTo(String search_value, int acc);

    List<Message> findMessageWithTag(String search_value, int acc);

    List<Message> findMessageFrom(String search_value, int acc);

    Date findLastDate(int folderId);
}
