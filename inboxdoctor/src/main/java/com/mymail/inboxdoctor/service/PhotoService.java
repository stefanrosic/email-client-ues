package com.mymail.inboxdoctor.service;

import com.mymail.inboxdoctor.entity.Photo;
import com.mymail.inboxdoctor.repository.PhotoRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PhotoService implements PhotoServiceInterface {

    private PhotoRepository photoRepository;

    public PhotoService(PhotoRepository photoRepository) {
        this.photoRepository = photoRepository;
    }

    @Override
    public Photo findOne(Integer photoId) {
        return photoRepository.findById(photoId).isPresent() ? photoRepository.findById(photoId).get() : null;
    }

    @Override
    public List<Photo> findAll() {
        return photoRepository.findAll();
    }

    @Override
    public Photo save(Photo photo) {
        return photoRepository.save(photo);
    }

    @Override
    public void remove(int photoId) {
        photoRepository.deleteById(photoId);
    }
}
