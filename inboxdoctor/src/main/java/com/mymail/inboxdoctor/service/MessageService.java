package com.mymail.inboxdoctor.service;

import com.mymail.inboxdoctor.entity.Message;
import com.mymail.inboxdoctor.repository.MessageRepository;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class MessageService implements MessageServiceInterface {

    private MessageRepository messageRepository;

    public MessageService(MessageRepository messageRepository) {
        this.messageRepository = messageRepository;
    }

    @Override
    public void removeMessagesByAccount(int accountId) {
        messageRepository.removeMessagesByAccount(accountId);
    }

    @Override
    public Message findOne(Integer messageId) {
        return messageRepository.findById(messageId).isPresent() ? messageRepository.findById(messageId).get() : null;
    }

    @Override
    public List<Message> findAll() {
        return messageRepository.findAll();
    }

    public List<Message> findAllByAccountIdAndFolderId(int accountId, int folderId) {
        return messageRepository.findAllByAccountIdAndFolderId(accountId, folderId);
    }

    @Override
    public Message save(Message message) {
        return messageRepository.save(message);
    }

    @Override
    public void remove(int messageId) {
        messageRepository.deleteById(messageId);
    }

    @Override
    public List<Message> findUnreadMessages(int accountId) {
        return messageRepository.findUnreadMessages(accountId);
    }

    @Override
    public void setRead(int messageID) {
        messageRepository.setRead(messageID);
    }

    @Override
    public List<Message> findMessagesByPassedValue(String search_value, int acc) {
        return messageRepository.findMessageForSearch(search_value, acc);
    }

    @Override
    public List<Message> findMessageWithEmailInBCC(String search_value, int acc) {
        return messageRepository.findMessageWithEmailInBCC(search_value, acc);
    }

    @Override
    public List<Message> findMessageWithEmailInCC(String search_value, int acc) {
        return messageRepository.findMessageWithEmailInCC(search_value, acc);
    }

    @Override
    public List<Message> findMessageWithEmailInTo(String search_value, int acc) {
        return messageRepository.findMessageWithEmailInTo(search_value, acc);
    }

    @Override
    public List<Message> findMessageWithTag(String search_value, int acc) {
        return messageRepository.findMessageWithTag(search_value, acc);
    }

    @Override
    public List<Message> findMessageFrom(String search_value, int acc) {
        return messageRepository.findMessageFrom(search_value, acc);
    }

    @Override
    public Date findLastDate(int folderId) {
        return messageRepository.findLastDate(folderId);
    }

	@Override
	public List<Message> findMessagesByPassedValueSubject(String search_value, int acc) {
		return messageRepository.findMessageForSearchSubject(search_value, acc);
	}
}
