package com.mymail.inboxdoctor.service;

import com.mymail.inboxdoctor.entity.Rule;

public interface RuleServiceInterface {

    Rule findOne(Integer ruleId);
}
