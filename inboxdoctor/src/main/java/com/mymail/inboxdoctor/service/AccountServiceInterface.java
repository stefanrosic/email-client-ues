package com.mymail.inboxdoctor.service;

import com.mymail.inboxdoctor.entity.Account;
import com.mymail.inboxdoctor.entity.User;

import java.util.List;

public interface AccountServiceInterface {

    Account findOne(Integer accountId);

    List<Account> findAll();

    Account save(Account account);

    void remove(int accountId);
    
    List<Account> findAllByUser(User user);
    
}
