package com.mymail.inboxdoctor.service;

import org.springframework.security.core.userdetails.UserDetailsService;

import com.mymail.inboxdoctor.entity.User;

public interface UserServiceInterface extends UserDetailsService{

	User findOne(Integer id);
	
	User findByUsername(String username);

	User save(User user);
	
	void insertAuthority(int user_id, int auth_id);

}
