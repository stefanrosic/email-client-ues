package com.mymail.inboxdoctor.service;

import com.mymail.inboxdoctor.entity.Account;
import com.mymail.inboxdoctor.entity.User;
import com.mymail.inboxdoctor.repository.AccountRepository;

import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AccountService implements AccountServiceInterface {

    private AccountRepository accountRepository;

    public AccountService(AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }

    @Override
    public Account findOne(Integer accountId) {
        return accountRepository.findById(accountId).isPresent() ? accountRepository.findById(accountId).get() : null;
    }

    @Override
    public List<Account> findAll() {
        return accountRepository.findAll();
    }

    @Override
    public Account save(Account account) {
        return accountRepository.save(account);
    }

    @Override
    public void remove(int accountId) {
        accountRepository.deleteById(accountId);
    }

	@Override
	public List<Account> findAllByUser(User user) {
		return accountRepository.findAllByUser(user);
	}
    
}
