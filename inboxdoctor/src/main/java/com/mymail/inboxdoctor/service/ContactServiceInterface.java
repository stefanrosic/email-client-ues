package com.mymail.inboxdoctor.service;

import com.mymail.inboxdoctor.entity.Contact;
import com.mymail.inboxdoctor.entity.User;

import java.io.IOException;
import java.util.List;

public interface ContactServiceInterface {

    Contact findOne(Integer contactId);

    List<Contact> findAllByUserId(int userId);

    List<Contact> findAll();

    Contact save(Contact contact);

    void remove(int contactId);
    
    List<Contact> findAllContacts(User user);
}
