package com.mymail.inboxdoctor.service;

import com.mymail.inboxdoctor.entity.Tag;

import java.util.List;

public interface TagServiceInterface {

    Tag findOne(Integer tagId);

    List<Tag> findAll();

    Tag save(Tag tag);

    void remove(int tagId);
}
