package com.mymail.inboxdoctor.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import com.mymail.inboxdoctor.security.AuthenticationEntryLocation;
import com.mymail.inboxdoctor.security.TokenAuthenticationFilter;
import com.mymail.inboxdoctor.security.TokenHelper;
import com.mymail.inboxdoctor.service.UserService;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter implements WebMvcConfigurer{
	
	@Autowired
	private AuthenticationEntryLocation authenticationEntryLocation;
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private TokenHelper tokenHelper;
	
	@Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

	@Override
	public void configure(AuthenticationManagerBuilder auth) {
		PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
		
		try {			
			auth.userDetailsService(userService).passwordEncoder(passwordEncoder);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

    @Bean
    public FilterRegistrationBean processCorsFilter() {
	    final UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
	    final CorsConfiguration config = new CorsConfiguration();
	    config.setAllowCredentials(true);
	    config.addAllowedOrigin("http://localhost:3000");
	    config.addAllowedHeader("Access-Control-Allow-Origin");
	    config.addAllowedHeader("Content-Type");
	    config.addAllowedHeader("Authorization");
		config.addAllowedHeader("Content-Disposition");
		config.addAllowedMethod("POST");
	    config.addAllowedMethod("GET");
	    config.addAllowedMethod("DELETE");
	    config.addAllowedMethod("PUT");
	    source.registerCorsConfiguration("/**", config);
	
	
	    final FilterRegistrationBean bean = new FilterRegistrationBean(new CorsFilter(source));
	    bean.setOrder(Ordered.HIGHEST_PRECEDENCE);
	    return bean;
    }
    
	@Override
	public void configure(WebSecurity web) throws Exception {
	    //web.ignoring().antMatchers(HttpMethod.OPTIONS);
		web.ignoring().antMatchers(HttpMethod.POST, "/authenticate");
		web.ignoring().antMatchers(HttpMethod.POST, "/api/user/add");
		web.ignoring().antMatchers(HttpMethod.POST, "/api/search/message/phrase/attach");
		web.ignoring().antMatchers(HttpMethod.GET, "/api/reindex");
		web.ignoring().antMatchers(HttpMethod.GET, "/api/reindex-contact");
		web.ignoring().antMatchers(HttpMethod.GET, "/api/reindex-messages");
		web.ignoring().antMatchers(HttpMethod.GET, "/api/reindex-attachment");
		web.ignoring().antMatchers(HttpMethod.GET, "/api/emails/download/attachment");
		web.ignoring().antMatchers(HttpMethod.GET, "/api/message/{filename}");
		web.ignoring().antMatchers("/v2/api-docs",
                "/configuration/ui",
                "/swagger-resources/**",
                "/configuration/security",
                "/swagger-ui.html",
                "/webjars/**");
	}
	
	@Override
	public void configure(HttpSecurity http) throws Exception {
		http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
		http.authorizeRequests().antMatchers(HttpMethod.POST, "/authenticate").permitAll();
		http.authorizeRequests().antMatchers(HttpMethod.POST, "/api/user/add").permitAll();
        http.authorizeRequests().anyRequest().authenticated();
        http.exceptionHandling().authenticationEntryPoint(authenticationEntryLocation);
        http.addFilterBefore(new TokenAuthenticationFilter(tokenHelper, userService), BasicAuthenticationFilter.class);

		http.csrf().disable();
	}
    
}
