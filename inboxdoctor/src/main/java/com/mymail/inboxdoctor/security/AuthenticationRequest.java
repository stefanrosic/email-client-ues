package com.mymail.inboxdoctor.security;

public class AuthenticationRequest {
	
	private String username;
	private String password;

	public String getUserName() {
		return username;
	}

	public void setUserName(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	public String toString() {
		return "TokenRequest [username=" + username + ", password=" + password + "]";
	}
	
}
