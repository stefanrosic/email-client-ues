package com.mymail.inboxdoctor.provider;

import java.io.Serializable;
import java.util.Date;

import org.springframework.stereotype.Component;

@SuppressWarnings("serial")
@Component
public class TimeProvider implements Serializable {

	public Date now() {
		return new Date();
	}
	
}
