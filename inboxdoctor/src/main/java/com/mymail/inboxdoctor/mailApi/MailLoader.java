package com.mymail.inboxdoctor.mailApi;

import com.mymail.inboxdoctor.entity.Account;
import com.mymail.inboxdoctor.service.AccountServiceInterface;
import com.mymail.inboxdoctor.service.ContactServiceInterface;
import com.mymail.inboxdoctor.service.FolderServiceInterface;
import com.mymail.inboxdoctor.service.MessageServiceInterface;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

@Component
public class MailLoader {

    private static final Logger log = LoggerFactory.getLogger(MailLoader.class);

    private AccountServiceInterface accountService;
    private MessageServiceInterface messageService;
    private ContactServiceInterface contactService;
    private FolderServiceInterface folderService;

    public MailLoader(AccountServiceInterface accountService,
                      MessageServiceInterface messageService,
                      ContactServiceInterface contactService,
                      FolderServiceInterface folderService) {
        this.accountService = accountService;
        this.messageService = messageService;
        this.contactService = contactService;
        this.folderService = folderService;
    }

    @Scheduled(fixedRate = 60000)
    public void startLoadingEmails() {
        try {
            log.info("Loading new emails from server and writting them to db");
            MailGetter mailGetter = new MailGetter(messageService, contactService, folderService);
            List<Account> accounts = accountService.findAll();
            for (Account account : accounts
            ) {
                mailGetter.loadEmails(account);
            }
            log.info("New emails loaded");
        } catch (Exception e) {
            log.info("Skipping for minute to pull new info from db");
        }
    }
}
