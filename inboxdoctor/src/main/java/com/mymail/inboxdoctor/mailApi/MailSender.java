package com.mymail.inboxdoctor.mailApi;

import com.mymail.inboxdoctor.entity.*;
import com.mymail.inboxdoctor.entity.Message;
import com.mymail.inboxdoctor.service.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.mail.*;
import javax.mail.internet.*;
import java.util.Properties;

public class MailSender {

    private static final Logger log = LoggerFactory.getLogger(MailSender.class);

    private final MessageServiceInterface messageService;
    private final ContactServiceInterface contactService;
    private final TagServiceInterface tagService;
    private final AttachmentServiceInterface attachmentService;
    private final FolderServiceInterface folderService;

    public MailSender(MessageServiceInterface messageService,
                      ContactServiceInterface contactService,
                      TagServiceInterface tagService,
                      AttachmentServiceInterface attachmentService,
                      FolderServiceInterface folderService) {
        this.messageService = messageService;
        this.contactService = contactService;
        this.tagService = tagService;
        this.attachmentService = attachmentService;
        this.folderService = folderService;
    }

    public void sendEmail(Message message, User user) {

        //Declare to
        StringBuilder builder = new StringBuilder();
        for (Contact t : message.getTo()) {
            contactService.save(t);
            builder.append(t.getEmail());
            builder.append(",");
        }
        String EMAIL_TO = builder.toString();

        //Declare cc
        builder.setLength(0);
        for (Contact t : message.getCc()) {
            contactService.save(t);
            builder.append(t.getEmail());
            builder.append(",");
        }
        String EMAIL_CC = builder.toString();

        //Declare bcc
        builder.setLength(0);
        for (Contact t : message.getBcc()) {
            contactService.save(t);
            builder.append(t.getEmail());
            builder.append(",");
        }
        String EMAIL_BCC = builder.toString();

        //Declare sender
        String EMAIL_FROM = message.getFrom().getEmail();

        //Mention user name and password as per your configuration
        Account account = message.getAccount();
        final String username = account.getUsername();
        final String password = account.getPassword();

        //Set properties and their values
        Properties prop = new Properties();
        prop.put("mail.smtp.host", account.getSmtp().split(":")[0]); //SMTP Host
        prop.put("mail.smtp.socketFactory.port", account.getSmtp().split(":")[1]); //SSL Port
        prop.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory"); //SSL Factory Class
        prop.put("mail.smtp.auth", "true"); //Enabling SMTP Authentication
        prop.put("mail.smtp.starttls.enable", "true");
        prop.put("mail.smtp.port", account.getSmtp().split(":")[1]); //SMTP Port
        prop.put("mail.smtp.ssl.enable", "true");

        //Create a Session object & authenticate uid and pwd
        Session session = Session.getInstance(prop, new javax.mail.Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(username, password);
            }
        });

        try {
            //Create MimeMessage object & set values
            MimeMessage messageObj = new MimeMessage(session);

            messageObj.setFrom(new InternetAddress(EMAIL_FROM));
            messageObj.setRecipients(javax.mail.Message.RecipientType.TO, InternetAddress.parse(EMAIL_TO));
            messageObj.setRecipients(javax.mail.Message.RecipientType.CC, InternetAddress.parse(EMAIL_CC));
            messageObj.setRecipients(javax.mail.Message.RecipientType.BCC, InternetAddress.parse(EMAIL_BCC));
            messageObj.setSubject(message.getSubject());

            //Declare Text Values
            builder.setLength(0);
            String messageContent = message.getContent() + "\n";
            message.getTags().forEach(t -> builder.append(t.getName()));
            messageContent += builder.toString();

            Multipart multipart = new MimeMultipart();

            for (Attachment attachment: message.getAttachments()) {
                MimeBodyPart filePart = new PreencodedMimeBodyPart("base64");
                filePart.setContent(attachment.getBase64(), attachment.getType());
                filePart.setFileName(attachment.getName());
                multipart.addBodyPart(filePart);
            }

            MimeBodyPart mimeBodyPart = new MimeBodyPart();
            mimeBodyPart.setText(messageContent);
            multipart.addBodyPart(mimeBodyPart);

            messageObj.setContent(multipart);
            Transport.send(messageObj);
            log.info("Message sent.");

            contactService.save(message.getFrom());
            for (Tag tag : message.getTags()
            ) {
                tag.setUser(user);
                tagService.save(tag);
            }
            message.setFolder(folderService.findSentFolderByAccount(account.getId()));
            message = messageService.save(message);
            for (Attachment attachment : message.getAttachments()
            ) {
                attachment.setMessage(message);
                attachmentService.save(attachment);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
