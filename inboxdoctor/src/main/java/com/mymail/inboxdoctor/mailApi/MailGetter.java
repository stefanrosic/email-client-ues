package com.mymail.inboxdoctor.mailApi;

import java.io.File;
import java.nio.file.Files;
import java.text.SimpleDateFormat;
import java.util.*;
import javax.mail.*;
import javax.mail.internet.*;

import com.mymail.inboxdoctor.entity.Account;
import com.mymail.inboxdoctor.entity.Contact;
import com.mymail.inboxdoctor.entity.Message;
import com.mymail.inboxdoctor.entity.Attachment;
import com.mymail.inboxdoctor.helper.Helper;
import com.mymail.inboxdoctor.lucene.indexer.Indexer;
import com.mymail.inboxdoctor.service.*;
import org.apache.tomcat.util.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class MailGetter {

    private static final Logger log = LoggerFactory.getLogger(MailGetter.class);

    //private final String TMP_PATH = "inboxdoctor/src/main/tmp"; //Ako ne radi otkomentarisi ovo
    private final String TMP_PATH = "src/main/resources/files";

    private MessageServiceInterface messageService;

    private ContactServiceInterface contactService;

    private FolderServiceInterface folderService;

    public MailGetter(MessageServiceInterface messageService,
                      ContactServiceInterface contactService,
                      FolderServiceInterface folderService) {
        this.messageService = messageService;
        this.contactService = contactService;
        this.folderService = folderService;
    }

    public void loadEmails(Account account) {
        try {

            Properties prop = new Properties();
            prop.put("mail.imap.host", account.getPop3imap().split(":")[0]);
            prop.put("mail.imap.port", account.getPop3imap().split(":")[1]);
            prop.put("mail.imap.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
            prop.setProperty("mail.imap.socketFactory.fallback", "false");
            prop.setProperty("mail.imap.socketFactory.port", account.getPop3imap().split(":")[1]);
            Session session = Session.getDefaultInstance(prop, null);
            Store store = session.getStore("imaps");
            store.connect("imap.googlemail.com", 993, account.getUsername(), account.getPassword());
            Folder emailFolder = store.getFolder("inbox");
            emailFolder.open(Folder.READ_ONLY);
            javax.mail.Message[] messages = emailFolder.getMessages();
            com.mymail.inboxdoctor.entity.Folder inbox = folderService.findInboxFolderByAccount(account.getId());
            Date lastDate = messageService.findLastDate(inbox.getId());
            if (lastDate == null)
                lastDate = new SimpleDateFormat("yyyy-MM-dd").parse("2000-01-01");

            for (javax.mail.Message message : messages) {
                if (message.getSentDate().after(lastDate)) {
                    log.info("Loading: Message title: " + message.getSubject());
                    Message temp = new Message();
                    Contact tempContact = new Contact(message.getFrom()[0].toString());
                    tempContact = contactService.save(tempContact);
                    temp.setFrom(tempContact);
                    Set<Contact> tempContatsSet = Helper.fromAddressestoContacts(message.getRecipients(javax.mail.Message.RecipientType.TO));
                    Set<Contact> tempContacts = new HashSet<>();
                    tempContatsSet.forEach(c ->  tempContacts.add(contactService.save(c)));
                    temp.setTo(tempContacts);
                    Set<Contact> tempContatsCCSet = Helper.fromAddressestoContacts(message.getRecipients(javax.mail.Message.RecipientType.CC));
                    Set<Contact> tempContactCCs = new HashSet<>();
                    tempContatsCCSet.forEach(c ->  tempContactCCs.add(contactService.save(c)));
                    temp.setCc(tempContactCCs);
                    Set<Contact> tempContatsBCCSet = Helper.fromAddressestoContacts(message.getRecipients(javax.mail.Message.RecipientType.BCC));
                    Set<Contact> tempContactBCCs = new HashSet<>();
                    tempContatsBCCSet.forEach(c ->  tempContactBCCs.add(contactService.save(c)));
                    temp.setBcc(tempContactBCCs);
                    temp.setDateTime(message.getSentDate());
                    temp.setSubject(message.getSubject());
                    temp.setAttachments(new HashSet<>());
                    String result = "";
                    try {
                        MimeMultipart body = (MimeMultipart) message.getContent();
                        for (int j = 0; j < body.getCount(); j++) {
                            MimeBodyPart bodyPart = (MimeBodyPart) body.getBodyPart(j);
                            if (bodyPart.isMimeType("text/plain")) {
                                result += "\n" + bodyPart.getContent();
                                break; // without break same text appears twice in my tests
                            } else if (bodyPart.isMimeType("text/html")) {
                                result = (String) bodyPart.getContent();
                            }
                            if (Part.ATTACHMENT.equalsIgnoreCase(bodyPart.getDisposition())) {
                                String fileName = bodyPart.getFileName();
                                File file = new File(TMP_PATH + File.separator + fileName);
                                bodyPart.saveFile(file);
                                byte[] bFile = Files.readAllBytes(file.toPath());
                                String sFile = Base64.encodeBase64String(bFile);
                                Attachment attachment = new Attachment();
                                attachment.setName(fileName);
                                attachment.setBase64(sFile);
                                attachment.setMessage(temp);
                                String type = bodyPart.getContentType();
                                type = type.split(";")[0].toLowerCase();
                                attachment.setType(type);
                                temp.getAttachments().add(attachment);
                                //file.delete();
                            }
                        }

                    } catch (Exception e) {
                        result = (String) message.getContent();
                    }
                    temp.setContent(result);
                    temp.setTags(new HashSet<>());
                    temp.setFolder(inbox);
                    temp.setAccount(account);
                    temp.setRead(false);
                    messageService.save(temp);
                }
            }
            emailFolder.close(false);
            store.close();
        } catch (Exception e) {
            log.warn("Skip loading mail for deleted account");
        }
    }
}