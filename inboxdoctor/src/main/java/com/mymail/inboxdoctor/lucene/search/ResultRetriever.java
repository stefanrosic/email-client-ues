package com.mymail.inboxdoctor.lucene.search;

import com.mymail.inboxdoctor.dto.AttachmentDTO;
import com.mymail.inboxdoctor.dto.ContactDTO;
import com.mymail.inboxdoctor.dto.MessageDTO;
import com.mymail.inboxdoctor.lucene.model.RequiredHighlight;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.elasticsearch.search.fetch.subphase.highlight.HighlightBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.searchbox.client.JestClient;
import io.searchbox.client.JestClientFactory;
import io.searchbox.client.config.HttpClientConfig;
import io.searchbox.core.Search;
import io.searchbox.core.SearchResult;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ResultRetriever {

    private static int maxHits = 10;
    private static final Logger log = LoggerFactory.getLogger(ResultRetriever.class);

    private static JestClient client;

    static {
        JestClientFactory factory = new JestClientFactory();
        factory.setHttpClientConfig(new HttpClientConfig
                .Builder("http://localhost:9200")
                .multiThreaded(true)
                .build());
        ResultRetriever.client = factory.getObject();

    }

    public static List<MessageDTO> getResultsMessage(org.elasticsearch.index.query.QueryBuilder query,
                                              List<RequiredHighlight> requiredHighlights) {
        if (query == null) {
            return null;
        }

        List<MessageDTO> results = new ArrayList<>();

        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        searchSourceBuilder.query(query);
        searchSourceBuilder.size(maxHits);

        HighlightBuilder highlightBuilder = new HighlightBuilder();
        highlightBuilder.field("from");
        highlightBuilder.field("to");
        highlightBuilder.field("dateTime");
        highlightBuilder.field("subject");
        highlightBuilder.field("content");
        highlightBuilder.field("tags");
        highlightBuilder.field("folder");
        highlightBuilder.field("account");
        highlightBuilder.field("account.username");
        highlightBuilder.field("bcc");
        highlightBuilder.field("cc");
        highlightBuilder.field("attachmentContent");

        highlightBuilder.preTags("<spam style='color:red'>").postTags("</spam>");
        highlightBuilder.fragmentSize(200);
        searchSourceBuilder.highlighter(highlightBuilder);
        Search search = new Search.Builder(searchSourceBuilder.toString())
                .addIndex("messages_doctor")
                .addType("message_doctor")
                .build();
        SearchResult result;
        try {
            result = client.execute(search);
            if(result.isSucceeded()) {
            	log.warn("Search succeed");
            }else {
            	log.warn("An error occured during searching: " + result.getErrorMessage());

            }
            List<SearchResult.Hit<MessageDTO, Void>> hits = result.getHits(MessageDTO.class);
            MessageDTO rd = new MessageDTO();

            for (SearchResult.Hit<MessageDTO, Void> sd : hits) {
                String highlight = "";
                for (String hf : sd.highlight.keySet() ) {
                    for (RequiredHighlight rh : requiredHighlights) {
                        if(hf.equals(rh.getFieldName())){
                            highlight += sd.highlight.get(hf).toString();
                        }
                    }
                }
                rd.setContent(sd.source.getContent());
                rd.setSubject(sd.source.getSubject());
                rd.setDateTime(sd.source.getDateTime());
                rd.setFrom(sd.source.getFrom());
                rd.setAccount(sd.source.getAccount());
                rd.setFolder(sd.source.getFolder());
                rd.setTags(sd.source.getTags());
                rd.setTo(sd.source.getTo());
                results.add(rd);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return results;
    }

    public static List<ContactDTO> getResultsContact(org.elasticsearch.index.query.QueryBuilder query,
                                                     List<RequiredHighlight> requiredHighlights) {
        if (query == null) {
            return null;
        }

        List<ContactDTO> results = new ArrayList<>();

        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        searchSourceBuilder.query(query);
        searchSourceBuilder.size(maxHits);

        HighlightBuilder highlightBuilder = new HighlightBuilder();
        highlightBuilder.field("firstName");
        highlightBuilder.field("lastName");
        highlightBuilder.field("email");
        highlightBuilder.field("note");

        highlightBuilder.preTags("<spam style='color:red'>").postTags("</spam>");
        highlightBuilder.fragmentSize(200);
        searchSourceBuilder.highlighter(highlightBuilder);
        Search search = new Search.Builder(searchSourceBuilder.toString())
                // multiple index or types can be added.
                .addIndex("contacts")
                .addType("contact")
                .build();
        SearchResult result;
        try {
            result = client.execute(search);
            if(result.isSucceeded()) {
                log.warn("Search succeed");
            }else {
                log.warn("An error occured during searching: " + result.getErrorMessage());

            }
            List<SearchResult.Hit<ContactDTO, Void>> hits = result.getHits(ContactDTO.class);
            ContactDTO rd = new ContactDTO();

            for (SearchResult.Hit<ContactDTO, Void> sd : hits) {
                String highlight = "";
                for (String hf : sd.highlight.keySet() ) {
                    for (RequiredHighlight rh : requiredHighlights) {
                        if(hf.equals(rh.getFieldName())){
                            highlight += sd.highlight.get(hf).toString();
                        }
                    }
                }
                rd.setFirstName(sd.source.getFirstName());
                rd.setLastName(sd.source.getLastName());
                rd.setEmail(sd.source.getEmail());
                rd.setNote(sd.source.getNote());
                results.add(rd);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return results;
    }

    public static List<AttachmentDTO> getResultsAttachment(org.elasticsearch.index.query.QueryBuilder query,
                                                           List<RequiredHighlight> requiredHighlights) {
        if (query == null) {
            return null;
        }

        List<AttachmentDTO> results = new ArrayList<>();

        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        searchSourceBuilder.query(query);
        searchSourceBuilder.size(maxHits);

        HighlightBuilder highlightBuilder = new HighlightBuilder();
        highlightBuilder.field("id");
        highlightBuilder.field("message");
        highlightBuilder.field("content");
        highlightBuilder.field("type");
        highlightBuilder.field("name");

        highlightBuilder.preTags("<spam style='color:red'>").postTags("</spam>");
        highlightBuilder.fragmentSize(200);
        searchSourceBuilder.highlighter(highlightBuilder);
        Search search = new Search.Builder(searchSourceBuilder.toString())
                // multiple index or types can be added.
                .addIndex("attachments")
                .addType("attachment")
                .build();
        SearchResult result;
        try {
            result = client.execute(search);
            if(result.isSucceeded()) {
                log.warn("Search succeed");
            }else {
                log.warn("An error occured during searching: " + result.getErrorMessage());

            }
            List<SearchResult.Hit<AttachmentDTO, Void>> hits = result.getHits(AttachmentDTO.class);
            AttachmentDTO rd = new AttachmentDTO();

            for (SearchResult.Hit<AttachmentDTO, Void> sd : hits) {
                String highlight = "";
                for (String hf : sd.highlight.keySet() ) {
                    for (RequiredHighlight rh : requiredHighlights) {
                        if(hf.equals(rh.getFieldName())){
                            highlight += sd.highlight.get(hf).toString();
                        }
                    }
                }
                rd.setId(sd.source.getId());
                rd.setContent(sd.source.getContent());
                rd.setMessage(sd.source.getMessage());
                rd.setName(sd.source.getName());
                rd.setType(sd.source.getType());
                results.add(rd);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return results;
    }
}
