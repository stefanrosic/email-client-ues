package com.mymail.inboxdoctor.lucene.search;

import com.mymail.inboxdoctor.lucene.model.SearchType;

import org.apache.lucene.queryparser.classic.ParseException;
import org.elasticsearch.common.unit.Fuzziness;
import org.elasticsearch.index.query.QueryBuilders;

public class QueryBuilder {

    private static int maxEdits = 1;

    public static org.elasticsearch.index.query.QueryBuilder buildQuery(SearchType searchType, String field, String value) throws IllegalArgumentException, ParseException {
        String errorMessage = "";
        if(field == null || field.equals("")){
            errorMessage += "Field not specified";
        }
        if(value == null){
            if(!errorMessage.equals("")) errorMessage += "\n";
            errorMessage += "Value not specified";
        }
        if(!errorMessage.equals("")){
            throw new IllegalArgumentException(errorMessage);
        }

        org.elasticsearch.index.query.QueryBuilder retVal = null;
        if(searchType.equals(SearchType.REGULAR)){
            retVal = QueryBuilders.matchQuery(field, value);
        }else if(searchType.equals(SearchType.FUZZY)){
            retVal = QueryBuilders.fuzzyQuery(field, value).fuzziness(Fuzziness.fromEdits(maxEdits));
        }else if(searchType.equals(SearchType.PREFIX)){
            retVal = QueryBuilders.prefixQuery(field, value);
        }else if(searchType.equals(SearchType.RANGE)){
            String[] values = value.split(" ");
            retVal = QueryBuilders.rangeQuery(field).from(values[0]).to(values[1]);
        }else{
            retVal = QueryBuilders.matchPhraseQuery(field, value);
        }

        return retVal;
    }
}
