package com.mymail.inboxdoctor.lucene.model;

public enum SearchType {
    REGULAR,
    FUZZY,
    PHRASE,
    RANGE,
    PREFIX,
    NESTED
}
