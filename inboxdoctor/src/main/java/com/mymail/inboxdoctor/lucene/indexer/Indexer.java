package com.mymail.inboxdoctor.lucene.indexer;

import com.mymail.inboxdoctor.dto.AttachmentDTO;
import com.mymail.inboxdoctor.dto.ContactDTO;
import com.mymail.inboxdoctor.dto.MessageDTO;
import com.mymail.inboxdoctor.dto.MessageListDTO;
import com.mymail.inboxdoctor.entity.Attachment;
import com.mymail.inboxdoctor.entity.Contact;
import com.mymail.inboxdoctor.entity.Message;
import com.mymail.inboxdoctor.lucene.indexer.handlers.*;

import io.searchbox.client.JestClient;
import io.searchbox.client.JestClientFactory;
import io.searchbox.client.JestResult;
import io.searchbox.client.config.HttpClientConfig;
import io.searchbox.core.*;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class Indexer {
		
    private static JestClient jestClient;
    
    private static final Logger log = LoggerFactory.getLogger(Indexer.class);

    private static Indexer indexer = new Indexer();

    public static Indexer getInstance(){
        return indexer;
    }
    
    private Indexer() {
        JestClientFactory factory = new JestClientFactory();
        factory.setHttpClientConfig(
        	      new HttpClientConfig.Builder("http://localhost:9200")
        	        .multiThreaded(true)
        	        .defaultMaxTotalConnectionPerRoute(2)
        	        .maxTotalConnection(10)
        	        .build());
        jestClient = factory.getObject();
    }
    
    public static boolean deleteContact(int id){
        JestResult result;
        try {
            result = jestClient.execute(new Delete.Builder(id+"")
                    .index("contacts")
                    .type("contact")
                    .build());
            log.warn("Deleting from index contact with id: " + id + " is succeeded: " + result.isSucceeded());
            if(result.isSucceeded())
                return true;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }

    public static boolean addContact(ContactDTO contactDTO){
        Index index = new Index.Builder(contactDTO).index("contacts").type("contact").id(contactDTO.getId()+"").build();
        JestResult result;
        try {
            result = jestClient.execute(index);
            log.warn("Indexing new contact with id: " + contactDTO.getId() + " is succeeded: " + result.isSucceeded());
            return result.isSucceeded();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }

    public int indexContacts(List<Contact> contacts) throws IOException {
        JestResult result = null;
        for(Contact contact: contacts) {
            Index index = new Index.Builder(new ContactDTO(contact)).index("contacts").type("contact").id(contact.getId()+"").build();
            result = jestClient.execute(index);
        }
        if(result.isSucceeded()) {
            log.warn("Indexing succeed");
        }else {
            log.warn("Indexing failed");
        }
        return 2;
    }

    public static boolean existContact(int id) throws IOException {
        DocumentResult d = jestClient.execute(new Get.Builder("contacts", id+"").build());
        return d.isSucceeded();
    }

    public int indexMessagess(List<Message> messages) throws IOException {
    	
    	JestResult result = null;
    	for(Message message: messages) {
    	    MessageDTO messageDTO = new MessageDTO(message);
            Index index = new Index.Builder(messageDTO).index("messages_doctor").type("message_doctor").id(message.getId()+"").build();
            result = jestClient.execute(index);
    	}
    	
    	if(result.isSucceeded()) {
    		log.warn("Indexing succeed");
    	}else {
    		log.warn("Indexing failed");
    	}

		return 2;
    }

    public static boolean addMessage(MessageDTO messageDTO) throws IOException {
        Index index = new Index.Builder(messageDTO).index("messages_doctor").type("message_doctor").id(messageDTO.getId()+"").build();
        JestResult result;
        try {
            result = jestClient.execute(index);
            log.warn("Inserting message in index message with message id: " + messageDTO.getId() + " is succeeded: " + result.isSucceeded());
            return result.isSucceeded();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }

    public static boolean existMessage(int id) throws IOException {
        DocumentResult d = jestClient.execute(new Get.Builder("messages_doctor", id+"").build());
        return d.isSucceeded();
    }

    public static boolean deleteMessage(int id){
        JestResult result;
        try {
            result = jestClient.execute(new Delete.Builder(id+"")
                    .index("messages_doctor")
                    .type("message_doctor")
                    .build());
            log.warn("Deleting contact from index with contact id: " + id + " is succeeded: " + result.isSucceeded());
            if(result.isSucceeded())
                return true;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }

    public int indexAttachments(List<Attachment> attachments) throws IOException {

        File dataDir = new File("src/main/resources/files");
        List<File> files = Arrays.asList(dataDir.listFiles());

        PDFHandler pdfHandler = new PDFHandler();
        String retVal = "";

        List<AttachmentDTO> attachmentDTOS = new ArrayList<>();
        for(Attachment attachment: attachments){
            AttachmentDTO attachmentDTO = new AttachmentDTO(attachment);
            attachmentDTOS.add(attachmentDTO);
        }

        for(File file: files) {
            for(AttachmentDTO attachmentDTO: attachmentDTOS){
                if (file.getName().endsWith(".pdf"))//  file.getName().equals(attachmentDTO.getName())) {
                {
                    if(file.getName().equals(attachmentDTO.getName())) {
                       attachmentDTO.setContent(pdfHandler.getIndexUnit(file).getText());
                    }
                }
            }
        }
        JestResult result = null;
        for(AttachmentDTO attachmentDTO: attachmentDTOS){
            Index index = new Index.Builder(attachmentDTO).index("attachments").type("attachment").id(attachmentDTO.getId()+"").build();
            result = jestClient.execute(index);
        }
        if(result.isSucceeded()) {
            log.warn("Indexing succeed");
        }else {
            log.warn("Indexing failed");
        }
        return 2;
    }

    public static boolean addAttachment(AttachmentDTO attachmentDTO){
        File dataDir = new File("src/main/resources/files");
        List<File> files = Arrays.asList(dataDir.listFiles());
        PDFHandler pdfHandler = new PDFHandler();

        for(File file: files) {
            if (file.getName().endsWith(".pdf")) {
                if(file.getName().equals(attachmentDTO.getName())) {
                    attachmentDTO.setContent(pdfHandler.getIndexUnit(file).getText());
                }
            }
        }

        Index index = new Index.Builder(attachmentDTO).index("attachments").type("attachment").id(attachmentDTO.getId()+"").build();
        JestResult result;
        try {
            result = jestClient.execute(index);
            log.warn("Inserting attachment in index with attachment id: " + attachmentDTO.getId() + " is succeeded: " + result.isSucceeded());
            return result.isSucceeded();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }

    public static boolean existAttachment(int id) throws IOException {
        DocumentResult d = jestClient.execute(new Get.Builder("attachments", id+"").build());
        return d.isSucceeded();
    }

    protected void finalize() throws Throwable {
        this.jestClient.shutdownClient();
    }
}
