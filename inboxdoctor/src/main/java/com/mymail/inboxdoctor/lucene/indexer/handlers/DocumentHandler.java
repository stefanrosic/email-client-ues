package com.mymail.inboxdoctor.lucene.indexer.handlers;

import com.mymail.inboxdoctor.lucene.model.IndexUnit;

import java.io.File;

public abstract class DocumentHandler {

    public abstract IndexUnit getIndexUnit(File file);
    public abstract String getText(File file);
}
