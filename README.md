Back-end aplikacija za email-client. Slanje i primanje email-ova. Aplikacija koju sam pravio za fakultet,
a sada je nadogradjujem podrskom za ElasticSearch.
Pre pokretanja aplikacije potrebno je kreirati schemu u MySQL Workbench-u:
`create schema inboxdb`

U projektu u target direktorijumu se nalaze inboxdoctor.war arhiva.
Jedan od načina osim onog sa pokretanjem projekta iz IDE jeste da se ta arhiva kopira u 
webapps direktorijum Tomcat servera, tj. da se uradi deploy na Tomcat.

Front-end za ovu aplikaciju je još uvek u fazi razvoja i radim ga koristeći React.js.
Korisnicko ime za prijavu kada se aplikacija pokrene:inbox, lozinka:stefan
Implementiran je swagger.ui, to možete pogledati na http://localhost:8080/swagger-ui.html
![Izgled swagger-a aplikacije](swagger.png)
![Telo odgovora nakon prosledjenih parametara u zahtevu sa parovima userName:inbox password:stefan](responsebody.png)
![Set token bearer to Authorization header](setAuthBearer.png)
![Uspesno autorizovani zahtevi :)](success.png)